USE DHSS;

IF OBJECT_ID('ResidencyEpisode') IS NOT NULL DROP TABLE ResidencyEpisode;
IF OBJECT_ID('ResidencyEpisodeStartReason') IS NOT NULL DROP TABLE ResidencyEpisodeStartReason;
IF OBJECT_ID('ResidencyEpisodeEndReason') IS NOT NULL DROP TABLE ResidencyEpisodeEndReason;
IF OBJECT_ID('Survey') IS NOT NULL DROP TABLE Survey;
IF OBJECT_ID('SurveyStatus') IS NOT NULL DROP TABLE SurveyStatus;
IF OBJECT_ID('Census') IS NOT NULL DROP TABLE Census;
IF OBJECT_ID('Residence') IS NOT NULL DROP TABLE Residence; 
IF OBJECT_ID('UserRole') IS NOT NULL DROP TABLE UserRole;
IF OBJECT_ID('User') IS NOT NULL DROP TABLE "User";
IF OBJECT_ID('Role') IS NOT NULL DROP TABLE "Role";
IF OBJECT_ID('Wall') IS NOT NULL DROP TABLE Wall;
IF OBJECT_ID('Floor') IS NOT NULL DROP TABLE "Floor";
IF OBJECT_ID('Lavatory') IS NOT NULL DROP TABLE Lavatory;
IF OBJECT_ID('Water') IS NOT NULL DROP TABLE Water;
IF OBJECT_ID('Person') IS NOT NULL DROP TABLE Person;
IF OBJECT_ID('Education') IS NOT NULL DROP TABLE Education;
IF OBJECT_ID('Occupation') IS NOT NULL DROP TABLE Occupation;
IF OBJECT_ID('Address') IS NOT NULL DROP TABLE Address;
IF OBJECT_ID('District') IS NOT NULL DROP TABLE District;
IF OBJECT_ID('Territory') IS NOT NULL DROP TABLE Territory;
 
-- User is a reserved word so we need to use quotes.
CREATE TABLE "User"
(
	UserId		BIGINT			PRIMARY KEY IDENTITY(1,1),	-- BIGINT holds integer data from -2^63 to 2^63-1. Storage size is 8 bytes.
	Username	VARCHAR(50)		NOT NULL UNIQUE,
	Password	VARCHAR(50)		NOT NULL,
	FirstName	VARCHAR(50)		NOT NULL,
	LastName	VARCHAR(50)		NOT NULL
);
GO

CREATE TABLE Role
(
	RoleId		BIGINT			PRIMARY KEY IDENTITY(1,1),
	Name		VARCHAR(50)		NOT NULL UNIQUE
);
GO

CREATE TABLE UserRole
(
	UserId		BIGINT			NOT NULL FOREIGN KEY REFERENCES "User" (UserId),		
	RoleId		BIGINT			NOT NULL FOREIGN KEY REFERENCES Role (RoleId),
	PRIMARY KEY(UserId, RoleId)
);
GO

CREATE TABLE Wall
(
	WallId		BIGINT			PRIMARY KEY IDENTITY(1,1),
	Name		VARCHAR(50)		NOT NULL UNIQUE
);
GO

CREATE TABLE Floor
(
	FloorId		BIGINT			PRIMARY KEY IDENTITY(1,1),
	Name		VARCHAR(50)		NOT NULL UNIQUE
);
GO

CREATE TABLE Water
(
	WaterId		BIGINT			PRIMARY KEY IDENTITY(1,1),
	Name		VARCHAR(50)		NOT NULL UNIQUE
);
GO

CREATE TABLE Lavatory
(
	LavatoryId	BIGINT			PRIMARY KEY IDENTITY(1,1),
	Name		VARCHAR(50)		NOT NULL UNIQUE
);
GO

CREATE TABLE Education
(
	EducationId		BIGINT			PRIMARY KEY IDENTITY(1,1),
	Name			VARCHAR(50)		NOT NULL UNIQUE
);
GO

CREATE TABLE Occupation
(
	OccupationId	BIGINT			PRIMARY KEY IDENTITY(1,1),
	Name			VARCHAR(50)		NOT NULL UNIQUE
);
GO

CREATE TABLE Person
(
	PersonId		BIGINT			PRIMARY KEY IDENTITY(1,1),
	FirstName		VARCHAR(50)		NOT NULL,
	LastName		VARCHAR(50)		NOT NULL,
	Sex				VARCHAR(1)		NOT NULL,
	BirthDate		DATE			NOT NULL,
	EducationId		BIGINT			NULL FOREIGN KEY REFERENCES Education (EducationId),
	OccupationId	BIGINT			NULL FOREIGN KEY REFERENCES Occupation (OccupationId)
);

CREATE TABLE Territory
(
	TerritoryId		BIGINT			PRIMARY KEY IDENTITY(1,1),
	Code			CHAR(1)			NOT NULL UNIQUE, -- CHAR allokerar enbart en byte
	Name			VARCHAR(50)		NOT NULL
);
GO

CREATE TABLE District
(
	DistrictId		BIGINT			PRIMARY KEY IDENTITY(1,1),
	Code			VARCHAR(2)		NOT NULL,
	Name			VARCHAR(50)		NOT NULL,
	TerritoryId		BIGINT			NOT NULL FOREIGN KEY REFERENCES Territory (TerritoryId)
	-- unique code + territoryid
);
GO

CREATE TABLE Address
(
	AddressId		BIGINT			PRIMARY KEY IDENTITY(1,1),
	DistrictId		BIGINT			NOT NULL FOREIGN KEY REFERENCES District (DistrictId),
	Block			TINYINT			NOT NULL,	-- TINYINT holds integer data from 0-255. Storage size is 1 byte.
	Code			CHAR(2)			NOT NULL
);
GO

CREATE TABLE Residence
(
	ResidenceId		BIGINT			PRIMARY KEY IDENTITY(1,1),
	HeadPersonId	BIGINT			NOT NULL FOREIGN KEY REFERENCES Person (PersonId),
	Electricity		BIT				NOT NULL,
	Bedrooms		TINYINT			NOT NULL,
	AddressId		BIGINT			NOT NULL FOREIGN KEY REFERENCES Address (AddressId),
	FloorId			BIGINT			NOT NULL FOREIGN KEY REFERENCES Floor (FloorId),
	WallId			BIGINT			NOT NULL FOREIGN KEY REFERENCES Wall (WallId),
	WaterId			BIGINT			NOT NULL FOREIGN KEY REFERENCES Water (WaterId),
	LavatoryId		BIGINT			NOT NULL FOREIGN KEY REFERENCES Lavatory (LavatoryId)
);
GO

CREATE TABLE ResidencyEpisodeStartReason
(
	ResidencyEpisodeStartReasonId	BIGINT			PRIMARY KEY IDENTITY(1,1),
	Name							VARCHAR(50)		NOT NULL UNIQUE
);
GO

CREATE TABLE ResidencyEpisodeEndReason
(
	ResidencyEpisodeEndReasonId		BIGINT			PRIMARY KEY IDENTITY(1,1),
	Name							VARCHAR(50)		NOT NULL UNIQUE
);
GO

CREATE TABLE ResidencyEpisode
(
	ResidencyEpisodeId	BIGINT		PRIMARY KEY IDENTITY(1,1),
	ResidenceId			BIGINT		NOT NULL FOREIGN KEY REFERENCES Residence (ResidenceId),
	PersonId			BIGINT		NOT NULL FOREIGN KEY REFERENCES Person (PersonId),
	StartDate			DATE		NOT NULL,
	StartReasonId		BIGINT		NOT NULL FOREIGN KEY REFERENCES ResidencyEpisodeStartReason (ResidencyEpisodeStartReasonId),
	EndDate				DATE		NULL,
	EndReasonId			BIGINT		NULL FOREIGN KEY REFERENCES ResidencyEpisodeEndReason (ResidencyEpisodeEndReasonId),
);
GO

CREATE TABLE Census
(
	CensusId		BIGINT			PRIMARY KEY IDENTITY(1,1),
	Name			VARCHAR(50)		NOT NULL UNIQUE,
	StartDate		DATE			NOT NULL,
	EndDate			DATE			NULL
);
GO

CREATE TABLE SurveyStatus
(
	SurveyStatusId	BIGINT			PRIMARY KEY IDENTITY(1,1),
	Name			VARCHAR(50)		NOT NULL UNIQUE
);
GO

CREATE TABLE Survey
(
	SurveyId			BIGINT			PRIMARY KEY IDENTITY(1,1),
	ResidenceId			BIGINT			NOT NULL FOREIGN KEY REFERENCES Residence (ResidenceId),
	InterviewerUserId	BIGINT			NOT NULL FOREIGN KEY REFERENCES "User" (UserId),
	SurveyStatusId		BIGINT			NOT NULL FOREIGN KEY REFERENCES SurveyStatus (SurveyStatusId),
	CensusId			BIGINT			NOT NULL FOREIGN KEY REFERENCES Census (CensusId)
);
GO

-- Populate lookup tables.
INSERT INTO Role (Name) VALUES ('Administrator');
INSERT INTO Role (Name) VALUES ('Quality Assurance');
INSERT INTO Role (Name) VALUES ('Supervisor');
INSERT INTO Role (Name) VALUES ('Data Entry');
GO

INSERT INTO Territory (Name, Code) VALUES ('Territory 1', 'T');
INSERT INTO Territory (Name, Code) VALUES ('Territory 2', 'S');
INSERT INTO Territory (Name, Code) VALUES ('Territory 3', 'M');
GO

INSERT INTO District (Name, Code, TerritoryId) VALUES ('District 1', 'D1', 1);
INSERT INTO District (Name, Code, TerritoryId) VALUES ('District 2', 'D2', 1);
INSERT INTO District (Name, Code, TerritoryId) VALUES ('District 3', 'D3', 2);
INSERT INTO District (Name, Code, TerritoryId) VALUES ('District 4', 'D4', 2);
INSERT INTO District (Name, Code, TerritoryId) VALUES ('District 5', 'D5', 3);
INSERT INTO District (Name, Code, TerritoryId) VALUES ('District 6', 'D6', 3);
GO

INSERT INTO Floor (Name) VALUES ('Ladrillo de Cer?mica');
INSERT INTO Floor (Name) VALUES ('Ladrillo de Cemento');
INSERT INTO Floor (Name) VALUES ('Ladrillo de barro');
INSERT INTO Floor (Name) VALUES ('Embaldosado');
INSERT INTO Floor (Name) VALUES ('Suelo');
GO

INSERT INTO Wall (Name) VALUES ('Ladrillos / Cemento');
INSERT INTO Wall (Name) VALUES ('Adobe / Taquezal');
INSERT INTO Wall (Name) VALUES ('Madera');
INSERT INTO Wall (Name) VALUES ('Palma');
INSERT INTO Wall (Name) VALUES ('Cart?n / Pl?stico / Metal / Ripios');
GO

INSERT INTO Water (Name) VALUES ('Tuber?a adentro');
INSERT INTO Water (Name) VALUES ('Tuber?a puesto comunal');
INSERT INTO Water (Name) VALUES ('Pozo propio');
INSERT INTO Water (Name) VALUES ('Pozo comunal');
INSERT INTO Water (Name) VALUES ('R?o / Quebrada');
INSERT INTO Water (Name) VALUES ('Agua comprada en barril / Bidones');
GO

INSERT INTO Lavatory (Name) VALUES ('Inodoro');
INSERT INTO Lavatory (Name) VALUES ('Excusado');
INSERT INTO Lavatory (Name) VALUES ('No tiene');
GO

INSERT INTO ResidencyEpisodeStartReason (Name) VALUES ('Baseline');
INSERT INTO ResidencyEpisodeStartReason (Name) VALUES ('Inmigration');
INSERT INTO ResidencyEpisodeStartReason (Name) VALUES ('Birth');
GO

INSERT INTO ResidencyEpisodeEndReason (Name) VALUES ('Emigration');
INSERT INTO ResidencyEpisodeEndReason (Name) VALUES ('Death');
GO

INSERT INTO Education (Name) VALUES ('1er Nivel');
INSERT INTO Education (Name) VALUES ('2do Nivel');
INSERT INTO Education (Name) VALUES ('3er Nivel');
INSERT INTO Education (Name) VALUES ('1er Grado');
INSERT INTO Education (Name) VALUES ('2do Grado');
INSERT INTO Education (Name) VALUES ('3er Grado');
INSERT INTO Education (Name) VALUES ('4to Grado');
INSERT INTO Education (Name) VALUES ('5to Grado');
INSERT INTO Education (Name) VALUES ('6to Grado');
INSERT INTO Education (Name) VALUES ('1er A?o');
INSERT INTO Education (Name) VALUES ('2do A?o');
INSERT INTO Education (Name) VALUES ('3er A?o');
INSERT INTO Education (Name) VALUES ('4to A?o');
INSERT INTO Education (Name) VALUES ('5to A?o');
INSERT INTO Education (Name) VALUES ('6to A?o');
INSERT INTO Education (Name) VALUES ('1er A?o Universitario');
INSERT INTO Education (Name) VALUES ('2do A?o Universitario');
INSERT INTO Education (Name) VALUES ('3er A?o Universitario');
INSERT INTO Education (Name) VALUES ('4to A?o Universitario');
INSERT INTO Education (Name) VALUES ('5to A?o Universitario');
INSERT INTO Education (Name) VALUES ('Profesional');
INSERT INTO Education (Name) VALUES ('No Aplicable');
INSERT INTO Education (Name) VALUES ('Analfabeto');
INSERT INTO Education (Name) VALUES ('Alfabetizado');
GO

INSERT INTO SurveyStatus (Name) VALUES ('Pending Approval');
INSERT INTO SurveyStatus (Name) VALUES ('Approved');
INSERT INTO SurveyStatus (Name) VALUES ('Rejected');
GO