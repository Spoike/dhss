﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using DHSS.Models;

namespace DHSS.DataAccess.SqlServer
{
	public class LavatoryDao : ILavatoryDao
	{
		public string ConnectionString { get; set; }

		public LavatoryDao()
		{
			ConnectionString = ConfigurationManager.ConnectionStrings["DHSS"].ConnectionString;
		}

		public List<Lavatory> FetchAll()
		{
			var wallList = new List<Lavatory>();

			using (var conn = new SqlConnection(ConnectionString))
			{
				string sql = @"SELECT * FROM Lavatory";

				SqlCommand command = new SqlCommand(sql, conn);
				
				try
				{
					conn.Open();

					// ExecuteReader() requires an open and available connection before being executed. Therefore we
					// need to invoke Open() first.
					SqlDataReader reader = command.ExecuteReader();

					while (reader.Read())
					{
						wallList.Add(new Lavatory {
							LavatoryId = long.Parse(reader["WallId"].ToString()),
							Name = reader["Name"].ToString()
						});
					}
				}
				catch(SqlException ex)
				{
					// Log exception.
				}
			}

			return wallList;
		}
	}
}