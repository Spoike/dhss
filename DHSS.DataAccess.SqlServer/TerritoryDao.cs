﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using DHSS.Models;

namespace DHSS.DataAccess.SqlServer
{
	public class TerritoryDao : ITerritoryDao
	{
		public string ConnectionString { get; set; }

		public TerritoryDao()
		{
			ConnectionString = ConfigurationManager.ConnectionStrings["DHSS"].ConnectionString;
		}

		public List<Territory> FetchAll()
		{
			List<Territory> territoryList = new List<Territory>();

			using (var conn = new SqlConnection(ConnectionString))
			{
				var sql = @"SELECT * FROM Territory";

				SqlCommand command = new SqlCommand(sql, conn);

				try
				{
					conn.Open();

					// ExecuteReader() requires an open and available connection before being executed. Therefore we
					// need to invoke Open() first.
					SqlDataReader reader = command.ExecuteReader();

					while (reader.Read())
					{
						territoryList.Add(new Territory {
							TerritoryId = long.Parse(reader["TerritoryId"].ToString()),
							Name = reader["Name"].ToString(),
							Code = reader["Code"].ToString()
						});
					}
				}
				catch (SqlException ex)
				{
					// Log exception.
				}
			}

			return territoryList;
		}
	}
}