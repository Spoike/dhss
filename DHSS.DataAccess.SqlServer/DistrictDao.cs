﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using DHSS.Models;

namespace DHSS.DataAccess.SqlServer
{
	public class DistrictDao : IDistrictDao
	{
		public string ConnectionString { get; set; }

		public DistrictDao()
		{
			ConnectionString = ConfigurationManager.ConnectionStrings["DHSS"].ConnectionString;
		}

		public List<District> FetchByTerritoryId(long territoryId)
		{
			List<District> districtList = new List<District>();

			using (var conn = new SqlConnection(ConnectionString))
			{
				string sql = @"
					SELECT District.DistrictId,
						   District.Name as DistrictName,
    					   District.Code as DistrictCode,
						   Territory.TerritoryId,
						   Territory.Name as TerritoryName,
						   Territory.Code as TerritoryCode
				      FROM District, Territory
					 WHERE Territory.TerritoryId = @territoryId
                       AND Territory.TerritoryId = District.TerritoryId
				";

				SqlCommand command = new SqlCommand(sql, conn);
				command.Parameters.Add("@territoryId", territoryId);
				
				try
				{
					conn.Open();

					// ExecuteReader() requires an open and available connection before being executed. Therefore we
					// need to invoke Open() first.
					SqlDataReader reader = command.ExecuteReader();

					District district;
					Territory territory;

					while (reader.Read())
					{
						district = new District();
						district.DistrictId = long.Parse(reader["DistrictId"].ToString());
						district.Name = reader["DistrictName"].ToString();
						district.Code = reader["DistrictCode"].ToString();
						
						territory = new Territory();
						territory.TerritoryId = long.Parse(reader["TerritoryId"].ToString());
						territory.Name = reader["TerritoryName"].ToString();
						territory.Code = reader["TerritoryCode"].ToString();
						
						district.Territory = territory;
						
						districtList.Add(district);
					}
				}
				catch(SqlException ex)
				{
					// Log exception.
				}
			}

			return districtList;
		}
	}
}