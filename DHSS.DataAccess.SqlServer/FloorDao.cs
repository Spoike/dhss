﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using DHSS.Models;

namespace DHSS.DataAccess.SqlServer
{
	public class FloorDao : IFloorDao
	{
		public string ConnectionString { get; set; }

		public FloorDao()
		{
			ConnectionString = ConfigurationManager.ConnectionStrings["DHSS"].ConnectionString;
		}

		public List<Floor> FetchAll()
		{
			var floorList = new List<Floor>();

			using (var conn = new SqlConnection(ConnectionString))
			{
				string sql = @"SELECT * FROM Floor";

				SqlCommand command = new SqlCommand(sql, conn);
				
				try
				{
					conn.Open();

					// ExecuteReader() requires an open and available connection before being executed. Therefore we
					// need to invoke Open() first.
					SqlDataReader reader = command.ExecuteReader();

					while (reader.Read())
					{
						floorList.Add(new Floor {
							FloorId = long.Parse(reader["FloorId"].ToString()),
							Name = reader["Name"].ToString()
						});
					}
				}
				catch(SqlException ex)
				{
					// Log exception.
				}
			}

			return floorList;
		}
	}
}