﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using DHSS.Models;

namespace DHSS.DataAccess.SqlServer
{
	public class WaterDao : IWaterDao
	{
		public string ConnectionString { get; set; }

		public WaterDao()
		{
			ConnectionString = ConfigurationManager.ConnectionStrings["DHSS"].ConnectionString;
		}

		public List<Water> FetchAll()
		{
			List<Water> waterList = new List<Water>();

			using (var conn = new SqlConnection(ConnectionString))
			{
				string sql = @"SELECT * FROM Water";

				SqlCommand command = new SqlCommand(sql, conn);
				
				try
				{
					conn.Open();

					// ExecuteReader() requires an open and available connection before being executed. Therefore we
					// need to invoke Open() first.
					SqlDataReader reader = command.ExecuteReader();

					while (reader.Read())
					{
						waterList.Add(new Water {
							WaterId = long.Parse(reader["WaterId"].ToString()),	
							Name = reader["Name"].ToString()               		
						});
					}
				}
				catch(SqlException ex)
				{
					// Log exception.
				}
			}

			return waterList;
		}
	}
}