﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using DHSS.Models;

namespace DHSS.DataAccess.SqlServer
{
	public class WallDao : IWallDao
	{
		public string ConnectionString { get; set; }

		public WallDao()
		{
			ConnectionString = ConfigurationManager.ConnectionStrings["DHSS"].ConnectionString;
		}

		public List<Wall> FetchAll()
		{
			List<Wall> wallList = new List<Wall>();

			using (var conn = new SqlConnection(ConnectionString))
			{
				string sql = @"SELECT * FROM Wall";

				SqlCommand command = new SqlCommand(sql, conn);
				
				try
				{
					conn.Open();

					// ExecuteReader() requires an open and available connection before being executed. Therefore we
					// need to invoke Open() first.
					SqlDataReader reader = command.ExecuteReader();

					while (reader.Read())
					{
						// We're using object initializers to set properties.
						wallList.Add(new Wall {
							WallId = long.Parse(reader["WallId"].ToString()),
							Name = reader["Name"].ToString()
						});
					}
				}
				catch(SqlException ex)
				{
					// Log exception.
				}
			}

			return wallList;
		}
	}
}