﻿using System;
using System.Data;
using System.IO;
using NUnit.Framework;
using System.Runtime.Serialization.Formatters.Binary;

namespace DHSS.Tests
{
	/*
	[TestFixture]
	public class DisconnectedClassesTest
	{
		
		[Test]
		public void CreateDataTable()
		{

			// DataTable håller samlingar med DataColumns som definierat tabellens schema och DataRow för raderna.
			// Vi börjar med att skapa en DataTable med namnet "People".
			DataTable people = new DataTable("Person");

			// Kontrollera att tabellnamnet är "Person".
			Assert.AreEqual("Person", people.TableName);

			// Vi kan ändra tabellnamnet i efterhand om vi vill
			people.TableName = "People";

			// Kontrollera att tabellnamnet är "People".
			Assert.AreEqual("People", people.TableName);

			// Innan vi kan börja lägga till DataRow till DataTable behöver vi definierat schemat för tabellen.
			// Detta görs genom att lägga till DataColumn:er. Vi lägger till följande:
			DataColumn id = new DataColumn("Id", typeof (int));
			DataColumn firstName = new DataColumn("FirstName");
			DataColumn lastName = new DataColumn("LastName");

			// Notera att vi anger datatypen för den första, men inte för resten. Om man inte anger en datatyp
			// kommer den sättas till "string".
			Assert.AreEqual(firstName.DataType, typeof (string));
			Assert.AreEqual(lastName.DataType, typeof (string));

			// Vi behöver lägga på begräsningar som säger vilken typ av data vi kan lagra i respektive kolumn.

			// Vi börjar med id-kolumnen. Denna kolumn ska vara primärnyckel för vår tabell. Detta innebär att den 
			// enbart får innehålla unika värden. Egenskapen Unique avgör om en kolumn får innehålla dubletter eller ej.
			// Standard är att dubletter tillåsts.
			Assert.AreEqual(id.Unique, false);

			// Standard är att inget värde behöver anges för kolumnen.
			Assert.AreEqual(id.AllowDBNull, true);

			// AutoIncrement har värdet false som standard. Detta innebär att vi själv ansvarar för att sätta ett unikt
			// värde för varje rad vi lägger in i tabellen.
			Assert.IsFalse(id.AutoIncrement);

			// Genom att sätta AutoIncrement till true kommer ett värde genereras automatiskt åt oss.
			id.AutoIncrement = true;

			// Två egenskaper som påverkar genereringen av värden är AutoIncrementSeed och AutoIncrementStep, där 
			// den förstnämnda har standardvärde 0 och den sistnämnda standardvärde 1:
			Assert.AreEqual(0, id.AutoIncrementSeed);
			Assert.AreEqual(1, id.AutoIncrementStep);

			// Notera att dessa värden aldrig persisteras i databasen. De är enbart till för internt bruk när 
			// vi håller informationen i minnet. Databasen kommer själv generera nummer och lagra dessa för 
			// kolumnen, om vi har satt AutoIncrement till true. När vi väljer att spara våra DataRow till databasen
			// kommer den att ersätta våra genererade nummer med egna och uppdatera vår minnes-modell, inklusive 
			// främmande nycklar.

			// Detta kan dock leda till problem. Låt säga vi skapar upp 100 nya rader i vårt program. I databasen
			// finns sedan tidigare redan 10 rader, som har värde 1-10 för primärnyckeln. Vi har även dessa värde
			// för de 10 första raderna i vår minnes-tabell. När vi skickar raderna till databasen för persistering
			// kommer den att se att senaste id som genererades var 10, och den genererar därför id nummer 11 för 
			// den första av de nya raderna. Denna får alltså id 11. Dock har vi redan en rad i minnet som har id 11
			// vilket leder till att ett undantag kastas.

			// För att lösa detta problemet sätter vi AutoIncrementSeed och AutoIncrementStep till -1, vilket kommer
			// göra att negativa nummer genereras och dessa kommer inte ligga i konflikt med de databasen genererar
			// eftersom databasen aldrig genererar negativa nummer.

			id.AutoIncrementSeed = -1;
			id.AutoIncrementStep = -1;

			// Vi skulle kunna sätta sätta Unique till true manuellt, men eftersom vi tänker utse kolumnen som primärnyckel
			// får tabellen kommer detta ske automatiskt. Vi börjar med att lägga till kolumnen till schemat:
			people.Columns.Add(id);

			// Vi sätter sedan denna till att vara primärnyckel för tabellen.
			people.PrimaryKey = new DataColumn[] {people.Columns["id"]};

			// Nu när vi satt kolumnen som en primärnyckel så ändrades Unique automatiskt till true, och likaså
			// ändrades AllowDBNull till false.
			Assert.IsTrue(id.Unique);
			Assert.IsFalse(id.AllowDBNull);

			// AutoIncrement har värdet false som standard. Skulle vi däremot peka ut kolumnen som en primärnyckel kommer
			// denna sättas till true automatiskt.
			Assert.IsTrue(id.AutoIncrement);

			// Vi hade även kunnat skriva på sättet nedan, då vi har tillgäng till en referens till
			// kolumnen:
			// people.PrimaryKey = new DataColumn[] { id };

			// Som vi ser är det en samling av kolumner, vilket gör det möjligt att definiera sammansatta nycklar.
			// Vi kommer nu se att Unique automatiskt har sats till true för id-kolumen.
			Assert.AreEqual(id.Unique, true);

			// Vi sätter nu egenskaper för kolumnen FirstName och LastName. Vi behöver inte specificera datatyp då 
			// standard är string om inget annat specificeras. Vi vill ha en begränsning på 50 tecken för både FirstName
			// och LastName. Standard är -1 för egenskapen MaxLength, vilket indikerar att ingen kontroll av maximal längd
			// utförs.
			Assert.AreEqual(firstName.MaxLength, -1);
			Assert.AreEqual(lastName.MaxLength, -1);

			// Vi sätter begräsning på maximal längd.
			firstName.MaxLength = 50;
			lastName.MaxLength = 50;

			// Standard är att kolumerna tillåter att man inte anger något värde för string. I detta fallet kommer de hålla
			// värdet DBNull, vilket kommer representeras av null i databasen.
			Assert.AreEqual(firstName.AllowDBNull, true);
			Assert.AreEqual(lastName.AllowDBNull, true);

			// Vi vill dock att det ska vara obligatoriskt att ange värde för både FirstName och LastName:
			firstName.AllowDBNull = false;
			lastName.AllowDBNull = false;

			// Vi kan specificera text-representationen för kolumner när de dyker upp i grafiska kontroller, så som t.ex.
			// grid-kontrollen. Standard är att det namn som angavs i konstruktorn när man skapade kolumen används:
			Assert.AreEqual(firstName.Caption, firstName.ColumnName);
			Assert.AreEqual(lastName.Caption, lastName.ColumnName);

			// Vi kan ändra så att ui-representationen är annan än namnet på kolumnen:
			firstName.Caption = "First Name";
			lastName.Caption = "Last Name";

			Assert.AreNotEqual(firstName.Caption, firstName.ColumnName);
			Assert.AreNotEqual(lastName.Caption, lastName.ColumnName);

			// Vi är nu klara med definitionen för FirstName och LastName. Vi lägger till dessa till tabellen:
			people.Columns.Add(firstName);
			people.Columns.Add(lastName);

			// Vi testar lägga till ett par rader. Skapandet av en rad måste ske inom kontextet av en tabell, då
			// raden måste känna till data schemat. För detta använder vi NewRow() hos DataTable:
			DataRow johnDoe = people.NewRow();
			johnDoe["FirstName"] = "John";
			johnDoe["LastName"] = "Doe";

			// Om vi försöker tilldela ett värde till en kolumn som inte finns kommer vi få ett System.ArgumentException.

			Assert.IsEmpty(people.Rows);

			people.Rows.Add(johnDoe);

			Assert.AreEqual(1, people.Rows.Count);
			Assert.AreEqual(-1, people.Rows[0]["id"]);

			// Notera att vi inte behövde ange något värde för id; detta genererades åt oss.

			// Det finns en överlagrad version av Add() som gör att vi slipper skapa upp en instans av DataRow.
			// Vi anger null för id då denna genereras automatiskt åt oss.
			people.Rows.Add(null, "Jane", "Doe");

			Assert.AreEqual(2, people.Rows.Count);
			Assert.AreEqual(-2, people.Rows[1]["id"]);

			// Vid detta läget går det att lägga till ytterligare kolumner, även om vi redan har börjat lägga till rader.
			DataColumn email = new DataColumn("Email");

			people.Columns.Add(email);

			// Vi kan skapa så kallade derived-kolumner, vilka använder ett uttryck för att skapa värdet i kolumnen.
			DataColumn fullName = new DataColumn("FullName");
			fullName.MaxLength = 100;
			fullName.Expression = "FirstName + ' ' + LastName";
			fullName.AllowDBNull = false;

			people.Columns.Add(fullName);

			Assert.AreEqual("John Doe", people.Rows.Find(-1)["FirstName"] + " " + people.Rows.Find(-1)["LastName"]);

			// Då AllowDBNull lämnades till standardvärde true, och eftersom FullName är en derived-kolumn, behöver vi
			// inte ange dessa när vi skapar upp en ny rad:
			people.Rows.Add(null, "Jack", "Doe");

			Assert.AreEqual(3, people.Rows.Count);

			
			::
			Hantera specialierade 
			typer

			DataTable - klassen
			låter oss 
			ha kolumner 
			vilkas typer 
			är specialiserade, vilket 
			möjliggör kolumner 
			innehålla
				XML 
			data eller 
			även instanser 
			av en 
			custom klass 
			som vi 
			skapat.

				En kolumn 
			vars datatyp 
			är en 
			referenstyp - förutom
			string 
			- kräver
			specialbehandling i 
			vissa fall, eftersom 
			en kolumns 
			datatyp är 
			en referestyp 
			och används 
			som en 
			primärnyckel eller 
			som en 
			Sort eller 
			RowFilter key, any 
			change to 
			the column 
			value must 
			involve assigning 
			a
			new object
			to the 
			column as opposed
			to just 
			changing properties 
			on the 
			columns existing 
			object.

				Tjhis
			assignment is required
			to trigger 
			the update 
			of th 
			einternal indexes 
			used
			by sorting, filtering, and 
			primary key 
			opertions.The following 
			code example 
			shows whoww 
			you can 
			assign a 
			customer Car 
			object to 
			a column 
			value:




			// Skapa och använda UDT:er.
			DataTable cars = new DataTable("Car");

			DataColumn vin = new DataColumn("Vin");
			vin.MaxLength = 23;
			vin.Unique = true;
			vin.AllowDBNull = false;
			cars.Columns.Add(vin);

			// UDT column
			DataColumn carColumn = new DataColumn("CarObject", typeof (Car));
			cars.Columns.Add(carColumn);

			// Add new DAtaRow by creating the DAtaRow first
			DataRow newAuto = cars.NewRow();
			newAuto["Vin"] = "1234566688ab";
			Car c = new Car {Make = "Chevy", Model = "Impala", Year = 2003};
			newAuto["CarObject"] = c;
			cars.Rows.Add(newAuto);
			Car theCar = (Car) cars.Rows[0]["CarObject"];
			textBox1.AppendText(string.Format("Car: {0} {1} {2} \r\n", theCars.Year, theCars.Make, theCars.Model));


			// Set the primary key.
			cars.PrimaryKey = new DataColumn[] {vin};
		}*/

	/*[Test]
	public void LoopOverDataRows()
	{

	::
		Loppa över 
		data med 
		DataTableReader - klassen

		Vi kan 
		loopa över 
		DAtaRow - objekt
		med DAtaTableReader, i 
		ett eller 
		flera
		DataTable - objekt.

			DataTAbleReader
		ger
		- stabil
		forward - only
		read - only
		sätt
			att 
		loopa över 
		DataRow - objekt.

			Om
		vi använder 
		DataTableReader - objekt
		för att 
		iterera över 
		rader i 
		ett DataTable 
		objekt kommer 
		vi inte 
		kunna använda 
		DataTablereader för 
		att
		modifiera rader, men 
		vi kommer 
		kunna kolmma 
		åt DataTable 
		objekt i 
		loop - koden
		för att 
		manipuerla dessa 
		(Rader).

			Kan
		även använda 
		DataTAbleReader för 
		att populera 
		många Web 
		kontroller utan 
		att behöver 
		skriva
		looping - kod
		eftersom många 
		list - baserade
		Webkontroller accpeterar 
		ett objekt 
		som impelemneterar 
		IDataReader
		interafcet.

			Om en 
		underliggade
		DataRow - objekt
		raderas eller 
		tas bort 
		från dess 
		data table 
		innan DataTableReader 
		når fram 
		till detta 
		Dataow - objekt,
		görs inget 
		försöker att 
		hämta
		DataRow - objektet.

			Om
		ett
		Datarow - objekt
		som readan 
		har lästs 
		raderas eller 
		ta bort, the 
		curent
		position is maintainted;
		there is no
		resulting
		shift in
		position.

			Om
		DataRow - objekt
		läggs till 
		i underliggade 
		DataTable objekt 
		medans
		DataTableReader loopar, kommer 
		dessa
		DAtaRow - objekt
		inkluderas i 
		DataTAblelReader iterantioner 
		enbart om 
		data raden 
		läggs till 
		efter nuvarande 
		position hos 
		DatATable
		readerobjektet.

			Datarow - objekt
		som läggs 
		inn innan 
		nuvanrade position 
		i
		DataTAbleReade - klassen
		inkluderas inte 
		i
		itterationen.

			DataTAbleReader har 
		Read()
		som körs 
		för att 
		ladda DataTAbleReader 
		med dataraden 
		(data
		row)
		vid
		nuvarande postiion, och 
		sen the 
		position is advanced.

			Om
		end of 
		data nåtts, kommer 
		Read()
		returnera
		null.Om
		vi försöker 
		läsa efter 
		att Read 
		nått eof 
		kommer altlid 
		null
		retuernas,
		även om 
		flera
		DAtaRow - objekt
		har lagts 
		till efter 
		detta.

			DataSet har 
		CreateDataReader()
		som returnerar 
		en isntans 
		av
		DAtaTAbleREAder.Om ett 
		DAtaset har 
		mer än 
		en tabell, kommer 
		DAtaTableReader läsa 
		rader från 
		första
		DataTAble - objketet,
		och vi 
		kan använda 
		NextResult()
		för att 
		fortsätta loopning 
		över samtliga 
		data - tabeller
		i
		datasetet.

			Följande kod 
		demonsterar användning 
		av DataTableReader 
		för att 
		loopa över 
		data table 
		och visas 
		Person objektets 
		Name - egenskap,
		och sedan 
		anropa NextResults 
		()
		för att 
		flytta til 
		nästa data 
		table och 
		visa något 
		annat
		där:

// Create an initial DataSet
		DataSet masterData = new DAtaSet("Sales");
		DAtaTable person = masterData.Tables.Add("Person");
		person.Columns.Add("Id", typeof (Guid));
		person.Columns.Add("Name", tupeof(string));
		person.PrimaryLEyu = new DAtacolumn[] {person.Columns["Id"]};

		DataTable part = masterData.Tables.Add("Part");
		parts.Columns.Add("Id", typeof (Guid));
		parts.Columns.Add("PartName", typeof (string));
		part.PrimaryLEy = new DataColumn[] {part.Columns["Id"]};

		for (int = 0;
		i < 100;
		i++)
		{
			person.Rows.ADd(Guid.NewGuid(), "Joe " + i);
			part.Rows.Add(Guid.NewGuid(), "Part " + i);


// Read the data in the DataTable
			DataTableREader rd = masterData.CreateDataReader();
			while (rd.Read())
			{
				textBox1.AppendText(rd["Name"].ToSTring() + "\r\n");
			}
			rd.NExtRsult();
			while (rd.Read())
			{
				textBox1.AppendText(rd["PartName"].ToString() + "r\n\");
			}

			DataTAbleREader ärver 
			från
			DbDataReader.


		}*/

	/*
	[Test]
	public void DataRowVersions ()
		{
			// DataTable kan hålla upp till tre versioner av varje DataRow-objekt: Original, Current, Proposed

			// DataAdapter känner av ändringar som gjorts sedan data laddades från datakällan och gör ändringar till disconnected data permanenta. 
			// Två versioner av varje rad underhålles för att DataAdapter ska kunna avgöra vilka rader som behöver persisteras.

			// Original-versionen innehåller värden som laddades in i raden. Current-versionen innehåller senaste versionen av vår data, 
			// inklusive ändringar som gjorts sedan data ursprungligen laddades.

			// För rader som nyligen skapades finns ingen Original-version. Skulle vi däremot anropa AcceptChanges() kommer värdena för
			// Current-versionen kopieras över till Original-versionen.

			// ADO.NET låter oss lägga en rad i edit mode vilket temporärt suspendar händelser för raden och låter användaren utföra
			// multipla ändringar på raden utan att trigga valideringsregler.

			// BeginEdit() lägger DataRow i edit mode, medans EndEdit() och CancelEdit() plockar ur raden ur edit mode. AcceptChanges()
			// tar även ut raden ur edit mode eftersom den implicit anropar EndEdit(). Detta gäller även för RejectChanges(), som implicit
			// anropar CancelEdit().

			// När vi lägger raden i edit mode kommer en Proposed-version skapas som innehåller ändringar som utförs när raden är i edit mode.
			// Om EndEdit() anropas kopieras ändringarna från Proposed till Current. Om CancelEdit() anropar kommer värdna i Proposed kastas.

			// Oavset vad, i båda fall, när editering är klar, kommer Proposed-versionen av raden inte längre finnas tillgänglig. Slutligen, en
			// Default radversion returnerar Current-versionen om raden inte håller på att editeras, eller om den gör det, returneras Proposed.

			// HasVersion() hos DataRow kan användas för att avgöra huruvida en specifik version av raden existerar. Om versionen existerar 
			// kan kolumnvärden för den hämtas genom att använda en av tre överlagrade indexers hos DataRow: dataRow[0, DataRowVersion.Original],
			// dataRow["FirstName", DataRowVersion.Original].

			// Current					Nuvarande, även efter ändring. Existerar i situationer förutom när DataRowState är "Deleted".
			// 
			// Default					Detta är standard version av RowState. Om RowState är Added, Modified eller Deleted, kommer
			//							default version vara Current. Om RowState är Detached kommer den vara Proposed.			
			// 
			// Original					Värdet som ursprungligen laddades in eller värdet vid tillfället då senaste AcceptChanges()
			//							kördes. Denna version är inte populerad förrän DataRowState blir "Modified", "Unchanged" eller "Deleted".
			//							Om RowState är "Deleted" kan vi ändå få tag på tidigare version via Original.
			//							Om DataRowState är "Added" kommer VersionNotFound kastas.
			//
			// Proposed					Värdet vid tillfället raden redigerades. Om RowState är "Deleted" kommer ett undantag
			//							att kastas. Om BeginEdit() inte har körts explicit, eller om BeginEdit har körts implicit genomatt redigera
			//							en detachad rad (en fristående/orphaned DataRow-objekt som inte lagts till en DataTable)
			//							kommer VersionNotFound kastas.
			// 

			// När en rad laddas från databasen, eller skapas för hand, finns det enbart en version - Current.
			// Varför inte Original? Original implikerar att raden har modifierats.

			DataTable people = CreateDHSSDataSet().Tables["People"];

			DataRow row = people.NewRow();

			row["FirstName"] = "John";
			row["LastName"] = "Doe";

			// Vid detta skedet kommer radversionen vara Proposed.
			Assert.IsTrue(row.HasVersion(DataRowVersion.Proposed));
			Assert.IsFalse(row.HasVersion(DataRowVersion.Current));
			Assert.IsFalse(row.HasVersion(DataRowVersion.Original));

			row["FirstName"] = "Jack";

			// Vad händer om vi ändrar FirstName till "Jack"?

			// Vid detta skedet kommer radversionen vara Proposed.
			Assert.IsTrue(row.HasVersion(DataRowVersion.Proposed));
			Assert.IsFalse(row.HasVersion(DataRowVersion.Current));
			Assert.IsFalse(row.HasVersion(DataRowVersion.Original));

			// Inget händer. Radversionen är fortfarande Proposed. Vi behöver lägga till raden till en tabell
			// och anropa AcceptChanges(). Notera att vi inte kan anropa AcceptChanges() för en rad som inte är 
			// kopplad till en tabell.
			people.Rows.Add(row);

			// Hur påverkade detta radversionerna?
			Assert.IsTrue(row.HasVersion(DataRowVersion.Current));
			Assert.IsFalse(row.HasVersion(DataRowVersion.Original));
			Assert.IsFalse(row.HasVersion(DataRowVersion.Proposed));

			// Vi ser att Proposed har gått över och blivit Current. Proposed-versionen upphörde därefter att existera.
			// Vi ser även att det inte finns någon Original-version vid detta skedet.

			// Vad händer om vi ändrar värdet för raden?
			row["FirstName"] = "Garfield";

			Assert.IsTrue(row.HasVersion(DataRowVersion.Current));
			Assert.IsFalse(row.HasVersion(DataRowVersion.Original));
			Assert.IsFalse(row.HasVersion(DataRowVersion.Proposed));

			// Inget. Vi har fortfarande enbart Current. För att få någon effekt måste vi anropa AcceptChanges():
			row.AcceptChanges();

			// Vi ser nu att vi har fått en Original-version, utöver Current-versionen.
			Assert.IsTrue(row.HasVersion(DataRowVersion.Current));
			Assert.IsTrue(row.HasVersion(DataRowVersion.Original));
			Assert.IsFalse(row.HasVersion(DataRowVersion.Proposed));

			// Vi kollar att FirstName är "Garfield" för Current-versionen:
			Assert.AreEqual("Garfield", row["FirstName"]);
			// Vi hade även kunnat skriva så här:
			// Assert.AreEqual("Garfield", people.Rows[0]["FirstName", DataRowVersion.Current]);

			// Vi kollar att FirstName även är "Garfield" för Original-versionen.
			Assert.AreEqual("Garfield", row["FirstName", DataRowVersion.Original]);

			// Om vi nu ändrar FirstName till "Stan", hur kommer detta påverka radversionerna?
			row["FirstName"] = "Stan";

			// Som vi ser nedan kommer detta inte påverka alls, dvs. vi har fortfarande en Current, en Original och 
			// saknar Proposed.
			Assert.IsTrue(row.HasVersion(DataRowVersion.Current));
			Assert.IsTrue(row.HasVersion(DataRowVersion.Original));
			Assert.IsFalse(row.HasVersion(DataRowVersion.Proposed));

			Assert.AreEqual("Garfield", row["FirstName", DataRowVersion.Original]);
			Assert.AreEqual("Stan", row["FirstName", DataRowVersion.Current]);

			// Vad händer om vi anropa AcceptChanges() vid detta skedet?
			people.AcceptChanges();

			// Vi har fortfarande ingen Proposed-version.
			Assert.IsTrue(row.HasVersion(DataRowVersion.Current));
			Assert.IsTrue(row.HasVersion(DataRowVersion.Original));
			Assert.IsFalse(row.HasVersion(DataRowVersion.Proposed));

			// Original har ändrats till "Stan". Current ligger kvar på "Stan".
			Assert.AreEqual("Stan", row["FirstName", DataRowVersion.Original]);
			Assert.AreEqual("Stan", row["FirstName", DataRowVersion.Current]);

			// Av detta drar vi slutsatsen att när AcceptChanges() anropas så kommer Original ta värdena som ligger i Current.

			// För att få Proposed-versionen behöver vi börja med att anropa BeginEdit().

			row.BeginEdit();

			Assert.IsTrue(row.HasVersion(DataRowVersion.Current));
			Assert.IsTrue(row.HasVersion(DataRowVersion.Original));
			Assert.IsTrue(row.HasVersion(DataRowVersion.Proposed));

			// Alla tre versioner har nu samma värde.
			Assert.AreEqual("Stan", row["FirstName", DataRowVersion.Original]);
			Assert.AreEqual("Stan", row["FirstName", DataRowVersion.Current]);
			Assert.AreEqual("Stan", row["FirstName", DataRowVersion.Proposed]);

			// Vi testar nu ändra FirstName.
			row["FirstName"] = "Cartman";

			// Vi har fortfarande tre versioner.
			Assert.IsTrue(row.HasVersion(DataRowVersion.Current));
			Assert.IsTrue(row.HasVersion(DataRowVersion.Original));
			Assert.IsTrue(row.HasVersion(DataRowVersion.Proposed));

			// Däremot har Proposed ändrats till "Cartman".
			Assert.AreEqual("Stan", row["FirstName", DataRowVersion.Original]);
			Assert.AreEqual("Stan", row["FirstName", DataRowVersion.Current]);
			Assert.AreEqual("Cartman", row["FirstName", DataRowVersion.Proposed]);

			// Om vi nu anropar RejectChanges() kommer Proposed-versionen att kastas.
			people.RejectChanges();

			Assert.IsTrue(row.HasVersion(DataRowVersion.Current));
			Assert.IsTrue(row.HasVersion(DataRowVersion.Original));
			Assert.IsFalse(row.HasVersion(DataRowVersion.Proposed));

			// Vi kör igen, och denna gången anropar vi AcceptChanges().
			row.BeginEdit();

			Assert.IsTrue(row.HasVersion(DataRowVersion.Current));
			Assert.IsTrue(row.HasVersion(DataRowVersion.Original));
			Assert.IsTrue(row.HasVersion(DataRowVersion.Proposed));

			row["FirstName"] = "Cartman";

			people.AcceptChanges();

			// Vi har nu tappat Proposed-versionen och Current ska vara satt till "Cartman", samt "Original"
			// ska ligga kvar på "Stan":

			Assert.IsTrue(row.HasVersion(DataRowVersion.Current));
			Assert.IsTrue(row.HasVersion(DataRowVersion.Original));
			Assert.IsFalse(row.HasVersion(DataRowVersion.Proposed));

			Assert.AreEqual("Stan", row["FirstName", DataRowVersion.Original]);
			Assert.AreEqual("Stan", row["FirstName", DataRowVersion.Current]);

		}
	*/
	/*
	[
		Test]
public
		void DataRowStates 
		()
		{

			// En DataRow går genom olika states. Dessa kan vi titta på och filtrera bland. Vi kan hämta nuvarande state
			// i form av ett värde från DataRowState, vilket är en enumerationen. DataRowState har följande värde:


			// Detached							Dataraden har skapats men inte lags till en data-tabell.
			// Added							Dataraden har skapats och lagts till en data-tabell.
			// Unchanged						Dataraden har inte förändats sedan senaste anropet till AcceptChanges().
			//									När AcceptChanges() anropas sätts data row state till denna state.
			// Modified							En committad rad har modifierats.
			// Deleted							En attachad datarad har raderats genom att använda Delete() hos DataRow.

			// Låt oss titta på hur en DataRow övergår mellan dessa states under dess livstid.

			// Vi använder en hjälpmetod för att snabbt skapa upp ett antal tabeller som vi kan använda oss av för att
			// köra våra tester.
			DataSet dhss = CreateDHSSDataSet();

			// Vi plockar ut People-tabellen.
			DataTable people = dhss.Tables["People"];

			// Vi försäkra oss om att vi har en instans.
			Assert.IsNotNull(people);
			Assert.AreEqual(0, people.Rows.Count);

			// Vi skapar upp en rad.
			DataRow row = people.NewRow();
			row["FirstName"] = "John";
			row["LastName"] = "Doe";

			// Vi har inte kopplat raden till en tabell och därför är dess state "Detached".
			Assert.AreEqual(DataRowState.Detached, row.RowState);

			// Vi lägger till raden till tabellen, vilket gör att state övergår till "Added"
			people.Rows.Add(row);

			Assert.AreEqual(DataRowState.Added, row.RowState);

			// Vi testar ändra värde för FirstName.
			row["FirstName"] = "John";

			// State kommer fortfarande vara "Added". Varför är den fortfarande "Added" istället för "Modified"?
			// RowState indikerar en handling som krävs för att skicka en uppdatering av data till databasen. Det 
			// faktum att FirstName ändrades är inte så viktigt som det faktum att tabellen behöver läggas till i databasen.
			Assert.AreEqual(DataRowState.Added, row.RowState);

			// Vi tar och återgår till ursprungliga version av samtliga rader i tabellen (i vårt fall har vi enbart
			// en rad). Detta kommer lämna raden i en "Detached" state, dvs. raden kommer inte längre vara kopplad till
			// tabellen.
			people.RejectChanges();

			Assert.AreEqual(DataRowState.Detached, row.RowState);

			// Vi lägger till raden på nytt, vilket innebär att RowState ändras till "Added" igen. Notera att när vi
			// körde RejectChanges() på tabellen så kastades även värden vi satt på DataRow, så vi behöver sätta dessa
			// på nytt.
			row["FirstName"] = "John";
			row["LastName"] = "Doe";
			people.Rows.Add(row);

			Assert.AreEqual(DataRowState.Added, row.RowState);

			// Vi accepterar ändringarna. Detta är som att committa. RowState kommer nu ändras till
			// unchanged eftersom vi inte har gjort några ändringar sedan raden committades. 
			people.AcceptChanges();

			Assert.AreEqual(DataRowState.Unchanged, row.RowState);

			// Om vi nu ändrar namn kommer RowState sättas till "Modified" då vi ändrar en rad som är en 
			// committad del av tabellen, tack vare att vi anropade AcceptChanges() tidigare.
			row["FirstName"] = "Jim";

			Assert.AreEqual(DataRowState.Modified, row.RowState);

			// Om vi committar ändringarna, genom att anropa AcceptChanges(), kommer RowState ändras till 
			// Unchanged igen. AcceptChange() finns på DataRow, DataTable och DataSet.
			people.AcceptChanges();

			Assert.AreEqual(DataRowState.Unchanged, row.RowState);

			// Vi ändrar FirstName på nytt och rullar sedan tillbaka till tidigare version.
			row["FirstName"] = "John";

			// Eftersom vi ändrade värdet för en committad rad kommer RowState sättas till "Modified".
			Assert.AreEqual(DataRowState.Modified, row.RowState);

			// Om vi rullar tillbaka kommer RowState sättas tillbaka till Unchanged.
			people.RejectChanges();

			Assert.AreEqual(DataRowState.Unchanged, row.RowState);

			// Om vi raderar raden kommer RowState sättas till "Deleted".
			row.Delete();

			Assert.AreEqual(DataRowState.Deleted, row.RowState);

			// Om vi rullar tillbaka kommer RowState sättas till "Unchanged".
			people.RejectChanges();

			Assert.AreEqual(DataRowState.Unchanged, row.RowState);

			/*
:: Återställa state genom att använda AcceptChanges() och RejectChanges()

I en vanlig dta miljö (efter data har laddats) sätts DataRow stat hos laddede rader till Added. Om vi anropar AcceptChanges() på data tabellen sätter vi row
state hos alla DataRow-objekt till Unchanged.

Om vi nu modifierar DataRow-objekt kommer deras row state ändras till Modified. När det är dags spaara data kan vi enkelt query DataTAble-objetk för dess ändringar gen om att använd a DtaTable GetChanges().  - returnerar ett DataTable-objekt populerat med enbart DataRow-objekt som har ändrats sedan senaste AcceptChanges() anropades. Enbart dessa
ändringar behöver skickas till data store.

När väl ändringar har skickats till data store måste vi ändra state hos DataRow till Unchagned, vilket indikerar att DataRow-objekt är synkroniserade med data store. Vi använder AccewptChanges) för detta.

Notera att executing AcceptChagnes() även gör att DAtaRow-bojekts Current data row version kopieras till DataRow-objektets Original-version.

NOTERA: Glöm inte anroa AcceptChanges()
Kom ihåg att anropa AcceptChanges() efter vi har gjort en lyckad uppdatering till databaseservern eftersom AcceptChagnes() markerar alla rader som Unchagned. Varför?
Föra tt rader nu är synkade med rader i databasservern.

RejectChanges() rullar tillbaka DataTable content tillbaka till där den
var vid skapadet eller när vi senast ekörde AcceptChanges().

NOTERA: AccpetChagnes() och RejectChanges() valigtvis återställer RowState till
Unchagned, men RejectChanges även kopierar DataRow objektets Original data rad version till DataRow-objektets Current data row version.


Använda SetAdded() och SetModified för att ändra RowSTate:

DataRow har SetAdded() och SetModified(); enablar en data row state att sättas med forcibly till
Added eller Modfieid. Dessa operaitioner är användbara när vi vill tvinga en datarad att lagras i en datastore på  ett annat sätt än den data store
från vilket data ursrunglignen laddades.

Exempelvis, om vi laddar en rad från en data store och vill skcika ddenna rad till en annan data store, kör vi SetAdded() för att få draden att likna en ny rad.

En DataAdapter objekt skickar ändringar till data store. När vi anstluter till destination data store
ser data adapter att  vår rad har row state Added, data adapter objektet kör edå en insert sats för att lägga till raden i destination data store.

Dessa metodern kan exekveras enbart på DataRowobjekt vars row state är Unchanged. Ett försöka tt köra dessa metoder på ett DataRow-objekt med en annan row state kastar ett InvalidOperationException.

Om SetAdded() körs kastar DataRow-objektet dess Original data rad version då DataRow objekt som har en row state Added aldrig innehåller en Original data rad version.

Om SetModfiied() anropas ändras DataRow-objektets RowState till Modfiied utan att modifiera Original eller Current dat a row version.

Radera Data Row, och Undelete
DataRow har Delete() där row state sätts för raden till Deleted. DataRow-bojekt som har row state Deleted indikerar rader som behöver raderas från data store. När DataRow-objektet raderas kastas Current och Proposed data rad versioner, men Original data row version ligger kvar.

Ibland behöver vi recover en raderar datarad. DataRow har ingen Undelete(). Vi kan dock avnända RejectChanges() för att rulla tillbkaa tidigare state när den raderade raden fortfarande var där.

Notera dock att om vi kör RejectChanges() kopierars Original data rad version till Current data rad version. Detta återståller DataRow-objektet till dess state första gången AcceptChagnes() kördes, men alla efterföljande ändringar som gjordes tpoå datat innan  raderingen har been discarded.

Enumerera Data Tabellen.

Det är möjligt att loopa genom rader och kolumner av data tabellen genom att avnända foreach. Följande kod visar hur rader och kolumner hos data table kan enumereras:

public void EnumerateTable(DataTable cars)
{
var buffer = new System.Text.StringBuilder();
	
foreach (DataColumn dc in cars.Columns)
{
	buffer.AppendFormat("{0,15} ", dc.ColumnName);
}
	
buffer.Append("\r\n");
	
foreach(DataRopw dr in cars.Rows) 
{
	if(dr.RowState == DataRowState.Deleted)
	{
		buffer.Append("Deleted Row");
	}
	else
	{
		foreach (DataColumn dc in cars.Columns)
		{
			buffer.AppendFormat("{0,15} ", dr[dc]);
		}
	}
	buffer.Append("\r\n");
}
textBox1.Text = buffer.ToString();
}
	*/


	/*
	}

[
	Test]
public
	void CopyAndCloneDataTable 
	()
	{
		//DataTable people = CreateDHSSDataSet().Tables["People"];

		// Vi kan kopiera schemat + data för en DataTable:
		//DataTable copy = people.Copy();

		// Vi kan kopiera enbart schemat; kallas att klona en tabell:
		//DataTable clone = people.Clone();

		// Vi kan använda ImportRow() för att kopiera en rad från en tabellen till en annan som har samma schema.
		// ImportRow() är användbart när Current- och Original-versionerna måste bibehållas. Exempelvis, efter redigering av en tabell 
		// kanske vi vill kopiera ändrade DataRow-objekt till en annan tabell, men vi vill bibehålla Original- och Current-versionerna.
		// ImportRow() hos DataTable kommer importera DataRow-objekt så länge en rad inte har samma primärnyckel som en existerande.
		// Om vi har dubletter kommer ConstraintException att kastas.

		// Importera rad från People-tabellen. Både Original- och Current-versionerna hänga med.
		//clone.ImportRow(people.Rows[0]);

	}
*/

	/*
	[
		Test]
public
		void DataView 
		()
		{
			// En DataView används som ett fönster in i en DataTable. Den låter oss sortera och filtrera rader. En DataTable kan ha många DataView-objekt 
			// tilldelade till sig. DataView låter oss titta på data på många olika sätt utan att behöva läsa in på nytt från databasen.

			// Egenskaperna Sort, RowFilter och RowStateFilter hos DataView kan kombineras efter behov.

			// Vi kan använda egenskaperna AllowDelete, AllowEdit och AllowNew hos DataView-objekt för att begränsa user input.

			// Internet är ett DataView-objekt i princip ett index. Vi kan förse med en sorteringsdefinition för att sortera indexet i en specifik ordning,
			// och vi kan förse med ett filter för att filtrera bland index-posterna.

			//DataTable people = CreateDHSSDataSet().Tables["People"];

			//DataView view = new DataView(people);
			//view.Sort = "LastName DESC, FirstName ASC";

			// En DataView exponerar en RowFilter- och en RowStateFilter-egenskap. RowFitler sätts till en SQL WHERE-sats utan ordet "WHERE". Följande visar
			// ett filter på LastName-kolumnen för FirstName som börjar med B Year som är nyare än 2003:
			//view = new DataView(people);
			//view.RowFilter = "FirstName like 'B%' and Year > 2003";

				


RowSTateFilter egenskapen förser med ett filter som tillämpaas på RowState egenskapen eller på varje data rad. Detta filter ger oss ett enkt metod för att hämnta specifika versioner av rader inom datatabellen. RowSTateFilter-egenskaper kräver användning av DataViewRowSTate enumrationsvärden.. Detta är en bit-flag enumeration, vilket
innebär att vi kan avnända bitwise OR operator (dvs. |) för att bygga compund filter. Exempelvis, default RowState filter värde är satt att visa flera states genom att kombinera Unchanged, Added och ModifieidCurrent numeration värden. Notera att denna kombination är så användbar att en dedikerad värde har definierast i enumerationen: CurrentRows:


DATAVIEWROWSTATE VÄRDE				BESKRIVNING

Added								Hämtar Current data rad version hos DataRow-objekt som har en rowstate satt till Added.
	
CurrentRows							Hämtar alla DAtaRow-objekt som har en Current data rad version. Motsvarar Added | Unchanged | ModifiedCurrent

Deleted								Hämtar Original data rad version hos DataRow-objekt som har en row state av Deleted.

ModifiedCurrent						Hämtar Current data row version hos DataRow-objekt  som har en row state av Modified

ModifiedOriginal					Hämtar Original data row version av DataRow objekt som har en row state av Modified.

None								Renmsar RowSTateFilter-egenskapen, vilket innebär att det inte finns något filter och vi kommer se alla rader (samtliga rader)

OriginalRows						Hämtar DataRow-objekt som har en Original dta row version

Unchanged							Hä'mtar DataRow-bojekt som har en row state av Unchanged




Följande kod visar en RowSTate filter soanvänds för att hänta enbart DataRow-bojekt som har en row state av Deleted:

DataView view = new DataView(cars);
view.RowFilter = "Make like 'B%' and Year > 2003"
view.RowStateFilter = DataViewRowState.Deleted;

Enumerera Data View


private void EnumerateView(DataView view)
{
var buffer = new System.Text.STringBuilder();
foreach (DataColumn dc in view.TAble.Columns)
{
	buffer.AppendFormat("{0,15} ", dc.ColumnNAme);
}
buffer.Append("\n\r");
foreach (DataRowView dr in view)
{
	foreach (DataColumn dc in view.TAble.Columns)
	{
		buffer.AppendFormat("{0,15} ", dr.Row[dc]);
	}
	buffer.Append("\r\n");
}
textBox1.Text = buffer.ToString();

}


Koden loopar över raderna i vyn. Data typen för rad är DataRowView, till motsats till DataRow, vilket
DataTAble hade.  Dessutom retrieving column information requires you to access the Table property
that the view has. 

:: Exportera ett DataView-objekt till en ny DataTable

Kan använda en DataView för att exportera data till en ny DataTable:

// Exportera kolumerna Vin, Make, Year från DataTable MyCarTable, till en ny: export.
// true är vad vi skickar in för "distinct" parametern; indikerar att enbart distinkta värden ska plockas, dvs. vi 
// filtrerar ut dubletter. Om false används skulle alla värden följa med.


DataTable export = view.ToTable("MyCarTable", true, "Vin", "Make", "Year");


DataTable clone = carsTable.Clone()
	 

		}





The data table requires the PrimeryKey property to be set so the DataTAble objekct can locate the rows to be updated. If you
need to generate a data row you can use the LoadDataRow() which accepts an array of objects, and a LoadOption
enumeration value. The possible values for the LoadOption enumeration:


LOADOPTION MEMBER						BESKRIVNING

OverwriteChanges						Skriver över ursprungling data rad version och nuvarande data rad version och ändrar row state till Unchanged. Nya rader kommer även de ha row state satt till Unchanged.

PreserveChanges							Skriver över ursprunglig data row version men modifierar inte nuvarande data rad version. Nya rader kommer ha row state satt till Unchagned.

Upsert									Skriver över nuvarande data row version men modifierar inte ursprungling data row version.
Nya rader kommer ha row state satt till Added. Rows som har en row state Unchagned kommer ha en row state av Unchanged om nuvarnade datarad version är
den samma som ursrunglig data row version, men om de är 
annlunda kommer row state sättas till Modified.



// Load DataRow, replacing exising contents, if existing.
cars.LoadDataRow(new object[] { "23423424", "Jeep", 2002}, LoadOption.OverwriteChanges);

Denna kod lägger till nya DataRow-objekt till cars-datatabellen. Notera att ingen än har persisterats till
databasen.

	


// Load DataRow, replacing exising contents, if existing.
cars.LoadDataRow(new object[] { "23423424", "Jeep", 2002}, LoadOption.OverwriteChanges);

Denna kod lägger till nya DataRow-objekt till cars-datatabellen. Notera att ingen än har persisterats till
databasen.
		*/
	/*


		[
			Test]
	public
			void SerializeAndDeserializeDataTable 
			()
			{
				// Vi kan serialisera DataTable till en XML- eller binärfil, eller ström.

				string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

				DataTable people = CreateDHSSDataSet().Tables["People"];
				people.Rows.Add(null, "John", "Doe");
				people.Rows.Add(null, "Jane", "Doe");

				// Skriv innehållet i People-tabellen till XML-fil.
				people.WriteXml(Path.Combine(path, "PeopleWithElements.xml"));

				// Detta kommer generera en XML-fil på skrivbordet med namnet People.xml som kommer se ut så här;

				// <?xml version="1.0" standalone="yes"?>
				// <DHSS>
				//    <People>
				//       <Id>-1</Id>
				//       <FirstName>John</FirstName>
				//       <LastName>Doe</LastName>
				//    </People>
				//    <People>
				//       <Id>-2</Id>
				//       <FirstName>Jane</FirstName>
				//       <LastName>Doe</LastName>
				//    </People>
				// </DHSS>

				// Ett XML-element kan inte innehålla space-tecken så kolumner med mellanslag, som t.ex. First Name kommer konverteras
				// till First_x0020_Name.

				// Vi kan tweaka XML output genom att förse med ett XML Schema (XSD) eller sätta egenskaper på data tabel och dess kolumner.
				// För att ändra namn på de återkommande elementen (<People> ovan) kan vi sätta egenskapen TableName.

				// För att skriva ut Id, FirstName, etc. som attribut istället kan vi sätta egenskapen ColumnMapping till MappingType.Attribute.
				// För att förhindra att en kolumn skrivs ut i XML-outputen, sätt ColumnMapping till MappingType.Hidden. Exempel:
				people.TableName = "Person";
				people.Columns["Id"].ColumnMapping = MappingType.Attribute;
				people.Columns["LastName"].ColumnMapping = MappingType.Hidden;

				// MappingType enumration medlemmar

				// Attribute					Kolumndata placeras i ett XMl attribut
				// Element						dfault; kolumndata placeras i ett xml ELEMENT
				// Hidden						kolumndata skickas inte till XMLfilen
				// SimpleContent				kolumndatalagras som text inom en <row>; med andra ord, data lagras som en text, fast utan elementtagg

				// NOTERA: ColumnMapping påverkar enbart genererad XML vid serialisering.

				people.WriteXml(Path.Combine(path, "PeopleWithElementsAndAttributes.xml"));

				// Detta ger:

				// <?xml version="1.0" standalone="yes"?>
				// <DHSS>
				//    <Person Id="-1">
				//       <FirstName>John</FirstName>
				//    </Person>
				//    <Person Id="-2">
				//       <FirstName>Jane</FirstName>
				//    </Person>
				// </DHSS>

				// Notera att datatyperna går förlorad, dvs. allt är string. Detta innebär att om vi skulle återställa vår DataTable från en sådan XML
				// kommer datatypen för alla kolumner sättas till string. Lösningen till detta problemet är att lagra XML Schemat tillsammans med vår data, 
				// vilket kan göras genom att ange enumerationen XmlWriteMode.WriteSchema när vi sparar:

				people.WriteXml(Path.Combine(path, "PeopleWithSchema.xml"), XmlWriteMode.WriteSchema);

				// Detta ger:
				// <?xml version="1.0" standalone="yes"?>
				// <DHSS>
				//   <xs:schema id="DHSS" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
				//     <xs:element name="DHSS" msdata:IsDataSet="true" msdata:MainDataTable="Person" msdata:UseCurrentLocale="true">
				//       <xs:complexType>
				//         <xs:choice minOccurs="0" maxOccurs="unbounded">
				//           <xs:element name="Person">
				//             <xs:complexType>
				//               <xs:sequence>
				//                 <xs:element name="FirstName" msdata:Ordinal="1">
				//                   <xs:simpleType>
				//                     <xs:restriction base="xs:string">
				//                       <xs:maxLength value="50" />
				//                     </xs:restriction>
				//                   </xs:simpleType>
				//                 </xs:element>
				//               </xs:sequence>
				//               <xs:attribute name="Id" msdata:AutoIncrement="true" msdata:AutoIncrementSeed="-1" msdata:AutoIncrementStep="-1" type="xs:int" />
				//               <xs:attribute name="LastName" type="xs:string" use="prohibited" />
				//             </xs:complexType>
				//           </xs:element>
				//         </xs:choice>
				//       </xs:complexType>
				//     </xs:element>
				//   </xs:schema>
				//   <Person Id="-1">
				//     <FirstName>John</FirstName>
				//   </Person>
				//   <Person Id="-2">
				//     <FirstName>Jane</FirstName>
				//   </Person>
				// </DHSS>

				// Vi ser att vi, utöver värdena, nu även har datatyperna och deras begränsningar.

				// Vi kan ladda en DataTable med dessa XML-filer och resulterande objekt kommer vara den samma som tidigare (i fallet
				// där XSD finns med i filen, eller inkluderas externt).

				DataTable peopleFromXml = new DataTable();
				peopleFromXml.ReadXml(Path.Combine(path, "PeopleWithSchema.xml"));

				Assert.AreEqual(2, peopleFromXml.Rows.Count);

				// Notera att derived-kolumner inte lagras, men kan ändå återställas då deras värde är beräknat.

			}
	*/
	/*

		[
			Test]
	public
			void MergingDataSets 
			()
			{
				// Scenario: Vi tar emot serialiserade DataSet-objekt från försäljare som är på resande fot. Vi vill
				// merga dessa med det som ligger på den centrala servern. Vi gör detta via Merge() och vi kan merga både
				// DataSet-, DataTable- och DataRow-objekt.

				// Vi börjar med att skapa ett initiellt DataSet.
				DataSet masterData = new DataSet("Sales");
				// Vi lägger till en tabell och definierar dess schema.
				DataTable people = masterData.Tables.Add("People");
				people.Columns.Add("Id", typeof (Guid));
				people.Columns.Add("Name", typeof (string));
				// Vi specificerar primärnyckel.
				people.PrimaryKey = new DataColumn[] {people.Columns["Id"]};
				// Vi lägger till en rad.
				people.Rows.Add(Guid.NewGuid(), "Joe");

				// Vi skapar en temporär kopia och utför ändringar.
				DataSet tempData = masterData.Copy();
				// Vi hämtar ut "Joe".
				DataTable sales = tempData.Tables["Sales"];
				DataRow joe = sales.Select("Name" = 'Joe')[0];

				// RowState för Joe's datarad är "Added".
				Guid joeId = (Guid) joe["Id"];

				// Vi modifierar namnet.
				joe["Name"] = "Joe in Sales";

				// Skapa en Order-tabell och lägg till ordrar för Joe.
				DataTable orders = tempData.Tables.Add("Orders");
				orders.Columns.Add("Id", typeof (Guid));
				orders.Column.Add("PersonId", typeof (Guid));
				orders.Column.Add("Amount", typeof (decimal));
				orders.PrimaryKey = new DataColumn[] {orders.Column["Id"]};
				orders.Rows.Add(Guid.NewGuid(), joeId, 100);

				// Vi mergar nu med master. Första parameter när tempData och andra är en Boolean som kallas preserveChanges, vilken
				// specificerar huruvida uppdateringar från tempDataSet ska skriva över ändringar i masterDataSet.

				// Exempelvis, Joe's datarad state i masterData är satt till Unchanged, så om preserveChanges är true, kommer Joe's namn att ändras
				// till "Joe in Sales", vilket inte kommer mergas in i masterData.

				/*

The last para is a MissingSchemaAction enumeration member.

The AddWithKey value is selected, which means the Sales data table and its data are added to masterData:


MissingSchemaAction Enumeration Members

MISSINGSCHEMAACTION VALUE			BESKRIVNING

Add									L'gger till nödvändig Dataable och DataColumn objekt to complete the schema
AddWithKey							Addes the necessary DataTable, DataColumn and PrimaryKey objects to comeplte the schema
Error							An exception is thrown if a data column does not exist in the data set being updated
Ignore								Ignores data that resides in data columns that are not in the data set being updated

When you uyse the Merge method, make sure the DataTable objects have a prmary key. Failure to set the PrimaryKey property of
the DataTable boject results in DataRow objects being appended rather than existing DataRow objects being moeified.

			}
			*/
	/*
			[
				Test]
		public
				void SerializeAndDeserializeDataSet 
				()
				{
					// Precis som med DataTable kan vi serialisera och deserialisera ett DataSet till XML- eller binär-fil eller ström.
					// Data-strömmen kan överföras över ett nätverk med många olika typer av protokoll, inkl. HTTP.

					DataSet dhss = CreateDHSSDataSet();

					dhss.Tables["People"].Rows.Add(null, "John", "Doe");
					dhss.Tables["People"].Rows.Add(null, "Jane", "Doe");

					Assert.AreEqual(2, dhss.Tables["People"].Rows.Count);

					dhss.Tables["Censuses"].Rows.Add(null, "Census 1", "This is census 1.");
					dhss.Tables["Censuses"].Rows.Add(null, "Census 2", "This is census 2.");

					Assert.AreEqual(2, dhss.Tables["Censuses"].Rows.Count);

					string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

					dhss.WriteXml(Path.Combine(path, "DHSS.xml"));

					// Detta är samma som att skriva:
					// dhss.WriteXml(Path.Combine(path, "DHSS.xml"), XmlWriteMode.IgnoreSchema);

					// Vi får följande:

					// <?xml version="1.0" standalone="yes"?>
					// <DHSS>
					//   <People>
					//     <Id>-1</Id>
					//     <FirstName>John</FirstName>
					//     <LastName>Doe</LastName>
					//   </People>
					//   <People>
					//     <Id>-2</Id>
					//     <FirstName>Jane</FirstName>
					//     <LastName>Doe</LastName>
					//   </People>
					//   <Censuses>
					//     <Id>-1</Id>
					//     <Name>Census 1</Name>
					//     <Description>This is census 1.</Description>
					//   </Censuses>
					//   <Censuses>
					//     <Id>-2</Id>
					//     <Name>Census 2</Name>
					//     <Description>This is census 2.</Description>
					//   </Censuses>
					// </DHSS>

					// Dvs. den skriver ut samlinga rader i People- och Censuses-tabellerna.
					// Namet på root-elementet tas från egenskapen DataSetName hos DataSet, vilket är något vi kan ändra om vi vill.


					/* 

				Ett annat alternativ är att nästala Parts elementen inuti Vnedor elmentet som har dom.
	Detta kan göras med Nested egenskapen hos DataRelation; sätt egenskapen till true;


	// Nästla data och xml attribute
	vendorData.RElations["vendor_part"].Nested = true;
	foreach (DataTable dt in vendorData.Tables)
	{
		foreach (DataColumn dc in dt.Columns)
		{
			if(dc.DataType != typeof(Guid))
				dc.ColumnMapping = MappingType.Attribute;
		}
	}
	vendorDat.WriteXml(desktopFileName("Vendors1.xml"), XmlWriteMode.IgnoreSchema);

	SE xml;

			  
	NOTERA: Om vi inte uttryckgen säger det kommer vi inte få med information om datatyper; kommer bli default string för alla.

	vendorDAta.WriteXml(desktopFileName("Vendors2.xml"), XmlWriteMode.WriteSchema);

	Nackdelen är att XML-filen blir mycket större. When few files are being generated for this data, this approach is acceptable,
	but if many files are bing created, it would be better to create
	a separate XSD file that can be loaded before the data. We can use the DAtaSet objects
	WriteXmlSchema() to extratct XML schema definition to a separate file:

	vendorData.WriteXmlSchema(desktopFileName("VendorSchema.xsd"));

				 

					//
					// Serialisera ett DataSet som ett DiffGram.
					//

					// Ett DiffGram är ett XML-dokument som innehåller all data från vårt DataSet-objekt, inklusive Original.

					// Serialisera som DiffGram genom att skicka in XmlWriteMode.DiffGram:
					dhss.WriteXml(Path.Combine(path, "DHSSDiffGram.xml"), XmlWriteMode.DiffGram);

					// När behöver vi serialisera till ett DiffGram? Tänk dig att vi skriver en applikation som ibland kopplar upp
					// sig mot en database för att synkronisera ett disconnected DataSet med information som finns i databasen. När vi
					// inte är uppkopplade mot databasen vill vi att vårt DataSet-objekt lagras lokalt.

					// Alla tidigare exempel serialiserade DataSet-objeketets nuvarande data, men inte ursprunglig data för varje rad (Original).
					// Detta innebär att denna information går förlorad när vi serialiserar data, information som behövs för att hitta ändrade
					// DataRow-objekt som behöver skickas till databasen för persistering.

					// DiffGram innehåller alla radversioner (Original, Current, (Proposed?).

					// 
					// Deserialisera ett DataSet från XML
					//

					// Tänk på att när schemat inte följer med behandlas all data som string, så vi bör först ladda schemat om det nu
					// ligger i en separat fil.

					// Läs in XML-fil i dataset:
					//dhss.ReadXmlSchema(Path.Combine(path, "DHSS.xsd"));
					//dhss.ReadXml(Path.Combine(path, "DHSSWithSchema.xml"), XmlReadMode.IgnoreSchema);
					/*
				// XmlReadMode enuermation members

				// Auto						XML source is examined by the ReadXml() and the appropriate mode is selected
				// 
				// DiffGram					IF XML file contains a DiffGram, the changes are applied to the dat set using the same semantics that the Merge method uses
				// 
				// Fragment					Reads the XML as a fragment. Fragments can containmultiple root elements. FOR XML in MS SQL SErver is an example of something that produces fragmanets
				// 
				// IgnoreSchema				Ignores any schema that is definied within the XML data file
				//
				// InferSchema					XML-filen läses, och schema (DataTable-objekt och DataColumn-objekt) skapas baserat on the d ata. If the 
	data set currently has a schema, the existing schema is used and extended to accommodate tablrs and columns that exist in the XML
	docuemtn but don't exist in the DataSet objekct schema. All data types of all DataCoclumn objecsta
	are treated as a string

				// InferTypedSchema			The XML file is read, and the schema is created based on the data. An attempt is made to identify the data type of each column, but if the data type cannot be identified, it will be a string
				// ReadSchema					Reads the XML file and looks for an embedded schema. If the data set already has DataTable objects iwth the same name, an exception is thrown. All other existing tables will remain.

	Inferring a schema simply means that the data set attempls to create a schema for th edata by looking for apttersn sof XML elements and attiubtes.
			
				 

					// Vi kan serialisera ett DataSet till binärdata. Detta kan vara användbart i fall XML-filen blir stor, vilket kan leda till problem
					// vid bearbetning när det gäller minne, lagringsutrymme, bandbred, etc. Om XML inte är ett krav kan serialisering till binärfil vara
					// ett alternativ.

					// Vi behöver importera följande:
					// using System.IO;
					// using System.Runtime.Serialization.Formatters.Binary;

					BinaryFormatter fmt;
					FileStream fs;

					fs = new FileStream(Path.Combine(path, "DHSSBinary.bin"), FileMode.Create);

					fmt = new BinaryFormatter();
					fmt.Serialize(fs, dhss);

					fs.Close();

					// DataSet DHSS serialiseras till en binärfil, men denna innehåller inbäddad XML. Denna typ av fil kallas för BinaryXml.
					// Om vi vill lagra som äkta binär data, lägg till följande högst upp:

					// dhss.RemotingFormat = SerializationFormat.Binary;
					// Resultatet blir en TrueBinary-fil, vilken kommer vara mindre.

					// NOTERA: Mindre objekt drar inga fördelar av att lagras som äkta binärfil. Stora objekt kan däremot bli 1/4 av 
					// ursprunglig (XML) storlek, och skulle därför gå 4 gånger snabbare att t.ex. överföra över nätverket.

					// Filen kommer nu inte längre innehålla inbäddad XML.

					// Deserialisera ett binärfil till DataSet.

					// Vi behöver importera följande:
					// using System.IO;
					// using System.Runtime.Serialization.Formatters.Binary;

					DataSet peopleData;

					fs = new FileStream(Path.Combine(path, "DHSSBinary.bin"), FileMode.Open);

					// BinaryFormatter lagrar schemat automatiskt när den serialiserar, så vi behöver inte ladda något sådant först.
					// Vi behöver inte heller ange om det är BinaryXml eller TrueBinary, detta kan den identifiera på egen hand.
					fmt = new BinaryFormatter();

					peopleData = (DataSet) fmt.Deserialize(fs);

					Assert.AreEqual(2, dhss.Tables.Count);
					Assert.AreEqual(2, dhss.Tables["People"].Rows.Count);
					Assert.AreEqual(2, dhss.Tables["Censuses"].Rows.Count);

					fs.Close();

				}

			[
				Test]
		public
				void CustomDataTypesInColumns 
				()
				{
				
	Notera: Car-klasen har attribute Serializable på sig, vilket är nödvändigt om vi vill serialisera data tablelen som innehååller detta värdet.
			
				}

			public
				DataSet CreateDHSSDataSet 
				()
				{
					DataSet dataSet = new DataSet("DHSS");

					// People table
					DataTable people = new DataTable("People");

					DataColumn id = new DataColumn("Id", typeof (int));
					id.AutoIncrement = true;
					id.AutoIncrementSeed = -1;
					id.AutoIncrementStep = -1;

					DataColumn firstName = new DataColumn("FirstName");
					firstName.AllowDBNull = false;
					firstName.MaxLength = 50;

					DataColumn lastName = new DataColumn("LastName");
					firstName.AllowDBNull = false;
					firstName.MaxLength = 50;

					people.Columns.Add(id);
					people.Columns.Add(firstName);
					people.Columns.Add(lastName);

					dataSet.Tables.Add(people);

					// Censuses table
					DataTable censuses = new DataTable("Censuses");

					DataColumn censusId = new DataColumn("Id", typeof (int));
					censusId.AutoIncrement = true;
					censusId.AutoIncrementSeed = -1;
					censusId.AutoIncrementStep = -1;

					DataColumn censusName = new DataColumn("Name");
					censusName.AllowDBNull = false;
					censusName.MaxLength = 50;

					DataColumn censusDescription = new DataColumn("Description");
					censusDescription.AllowDBNull = false;
					censusDescription.MaxLength = 50;

					censuses.Columns.Add(censusId);
					censuses.Columns.Add(censusName);
					censuses.Columns.Add(censusDescription);

					dataSet.Tables.Add(censuses);

					return dataSet;
				}

			[
				Test]
		public
				void ConnectDataTables 
				()
				{
					Koppla ihop 
					Tabeller med 
					DataRelation - objekt
					DataRelation - objekt
					jointar
					DataTable - objekt
					i samma 
					data
					set.Joinning DataTable 
					objekt skapar 
					en en 
					sökväg från 
					en kolumn 
					hos en 
					DataTable - objekt
					till en 
					kolumn i 
					en
					annan.
						Detta DataRleationobjekt 
					kan travaseras 
					programmatiskt från 
					förälder data 
					table till 
					child data 
					table eller 
					vice versa, vilket 
					enablar navigration 
					mellan
					DAtaTAble - objekten.

	// Lägg till vendors och parts.
						DataRow
					vendorRow = null;
					vendorRow = vendors.NewRow();
					Guid vendorId = Guid.NewGuid();
					vendorRow["Id"] = vendorId;
					vendorRow["Name"] = "Tailspin Toys";
					vendors.Rows.Add(vendorRow);

					DataRow partRow = null;
					partRow = parts.NewRow();
					partsRow["Id"] = Guid.NewGuid();
					partsRow["VendorId"] = vendorId;
					partsRow["PartCode"] = "WGT1";
					partsRow["PartDescription"] = "Widget 1 Description";
					partsRow["Cost"] = 10.00;
					partsRow["RetailPrice"] = 12.32;
					parts.Rows.ADd(partRow);

					partRow = parts.NewRow();
					partsRow["Id"] = Guid.NEwGuid();
					partsRow["VendorId"] = vendorId;
					partsRow["PartCode"] = "WGT2";
					partsRow["PartDescription"] = "Widget 2 Description";
					partsRow["Cost"] = 9.00;
					partsRow["RetailPrice"] = 11.32;
					parts.Rows.ADd(partRow);

	// Navigate parent to child

					textBox1.AppendText("\r\nParent to Children\r\n");
					DataRow[] childParts = vendorRow.GetChildRows("vendors_parts");
					foreach (DataRow dr in childParts)
					{
						textBox1.AppendText(string.Format("Part {0} from {1}\r\n", dr["PartCode"], vendorRow["Name"]));
					}

					textBox.Append("-----------------------\r\n");

	// Navigate child to partent
					DatarowRparentRow = parts.Rows[1].GetParentRow("vendors_parts");

					::
					Skapa Primary 
					och Foreign 
					KEy Constraints 

					Kan skapa 
					DataRelation - objekt
					med eller 
					utan unika 
					och främmande 
					nyckelkonstraints för 
					syfte att 
					navigrea
						mellan 
					förälder och 
					barn
					DataTAble - objekt.

						The
					DataRelation class
					provides a 
					constructor that 
					enables the 
					creation of 
					a unique 
					constraint on 
					the parent 
					DataTable objewkt 
					and a 
					foreign
						key 
					constraint on 
					the child 
					DataTAble
					objekt.

						Tehse constraints 
					enforce data 
					integgrity by 
					ensuring that 
					a partent 
					DataRow objekt 
					exists
					for any child 
					DataRow
					objekt.The following 
					code demonstrates 
					the creation 
					of the 
					DataRelation objekt 
					named
						vendor_part, passing 
					troe to 
					create constinra 
					if
					they dont 
					' 
					already
					exist.

						NOTE:
					Foreign keys 
					and
					null
					values

						Om 
					en främmande 
					nyckel begräsnings 
					är satt 
					till en 
					DataColumn objekt 
					som tillåter 
					null
					kan denna 
					barn data 
					rad existera 
					utan att 
					ha en 
					förälder
					DataRow - objekt.I
					vissa
						situationer 
					kan detta 
					vara önskvärt 
					eller till 
					och med 
					ett krav, men 
					i
					andra sitationer, kanske 
					inte.
						Var säker 
					att verifiera 
					att AlowDBNull 
					egenskapen hos 
					DataColumn objekten 
					som används 
					på frsom 
					används för 
					främmande nycklar 


					vendorData.Relations.Add("vendors_parts", vendors.Columns["Id"], parts.Columns["VendorId"], true);


					s
						::Cascading Deletes 
					och Cascading 
					Updates

						*
						*
						*
						En 
					främmande nyckel 
					constrinta försäkrar 
					att en 
					barn
					DataRow - objektinte
					kan läggas 
					till såvida 
					en giltig 
					partn
					DataRow - objekt
					existierar.
						I vissa 
					situationer är 
					det önskvärt 
					att tvina 
					raderingen av 
					barn
					DtaRow - objekt
					när förälder 
					DataRow - objekt
					raderas.Vi kan 
					gröas detta 
					genom
						att 
					sätta DeleteRule 
					till Cascade 
					hos ForeignKeyCoinstraint 
					contraint hos 
					barn - tabellen
					motsvarande
					relationen.

						Cascade är 
					default
					inställningar.

						As
					with deleting, on 
					some occasions, you 
					'l
					l want 
					to cascade 
					changes to 
					a
					unique
					key in
					the parent 
					DataRow - objekt
					to the 
					child DataRow 
					objektcts forgien 
					key.You can 
					set ChangeRule 
					to a 
					member
						of 
					the Rule 
					enumeration to 
					get the 
					appropriate
					behavior.

						RULE VALUE 
					DESCRIPTION

					Cascade Dfault;
					raderar eller 
					uppdaterar barn 
					DataRow - objekt
					när
					DataRow - objekt
					raderas eller 
					dess unikqa 
					nyckel
					ändras.

						None Kastar 
					en InvalidConstraintExcpetion 
					om förälder 
					DataRow - objekt
					raderas eller 
					dess unika 
					nycke
					ändras.

						SetDefault Sätter 
					the foreign 
					key column 
					(s) value
					to the 
					default
					value of 
					the DataColumn 
					object 
					(s)
					if
					the parent 
					Datarow objekt 
					id deleted 
					or its 
					unique
					key is changed.

						SetNull
					Sets the 
					foreign key 
					column(s)
					value to 
					DBNull
					if
					the parent 
					DataRow - objekt is deleted
					or its 
					unique
					key is changed.


						The
					default
					setting
					for a ForeignKeyConstraint 
					objekct
					's
					DeleteRule
					property is Rule.Cascade.The
					following
						code 
					sample shows 
					how to 
					force an 
					InvalidConstraintException to 
					be thrown 
					when a 
					parent that 
					has
					children is deleted,
					or when 
					a
					child is being
					added that 
					has no 
					parent.

	// SEt delete rule
						ForeignKeyConstraint fk = (ForeignKeyConstarint) parts.Constraints["vendors_parts"];
					fk.DeleteRule = Rule.None;


				}

			static
				void Write 
				(string
				line)
				{
					System.Console.WriteLine(line);
				}
			}

			[Test]
			public void LoadData()
			{
	
	The data table requires the PrimeryKey property to be set so the DataTAble objekct can locate the rows to be updated. If you
	need to generate a data row you can use the LoadDataRow() which accepts an array of objects, and a LoadOption
	enumeration value. The possible values for the LoadOption enumeration:

	LOADOPTION MEMBER						BESKRIVNING
	OverwriteChanges						Skriver över ursprungling data rad version och nuvarande data rad version och ändrar row state till Unchanged. Nya rader kommer även de ha row state satt till Unchanged.
	PreserveChanges							Skriver över ursprunglig data row version men modifierar inte nuvarande data rad version. Nya rader kommer ha row state satt till Unchagned.
	Upsert									Skriver över nuvarande data row version men modifierar inte ursprungling data row version.
	Nya rader kommer ha row state satt till Added. Rows som har en row state Unchagned kommer ha en row state av Unchanged om nuvarnade datarad version är
	den samma som ursrunglig data row version, men om de är 
	annlunda kommer row state sättas till Modified.

	// Load DataRow, replacing exising contents, if existing.
	cars.LoadDataRow(new object[] { "23423424", "Jeep", 2002}, LoadOption.OverwriteChanges);

	Denna kod lägger till nya DataRow-objekt till cars-datatabellen. Notera att ingen än har persisterats till
	databasen.

	// Load DataRow, replacing exising contents, if existing.
	cars.LoadDataRow(new object[] { "23423424", "Jeep", 2002}, LoadOption.OverwriteChanges);

	Denna kod lägger till nya DataRow-objekt till cars-datatabellen. Notera att ingen än har persisterats till
	databasen.
			
		static void DataRowHasRowStates()
		{
			}
	


		[Serializable]
		public class Car
		{
			public int Year { get; set; }
			public string Make { get; set; }
			public string Model { get; set; }
		}
	}

	/*


		
			/*

	:: Återställa state genom att använda AcceptChanges() och RejectChanges()

	Kan använda AcceptChanges() för att återställa DataRow state till Unchagned. Denna metod hittar vi på
	DataRow, DataTable, DataSet-objekt.

	I en vanlig dta miljö (efter data har laddats) sätts DataRow stat hos laddede rader till Added. Om vi anropar AcceptChanges() på data tabellen sätter vi row
	state hos alla DataRow-objekt till Unchanged.

	Om vi nu modifierar DataRow-objekt kommer deras row state ändras till Modified. När det är dags spaara data kan vi enkelt query DataTAble-objetk för dess ändringar gen om att använd a DtaTable GetChanges().  - returnerar ett DataTable-objekt populerat med enbart DataRow-objekt som har ändrats sedan senaste AcceptChanges() anropades. Enbart dessa
	ändringar behöver skickas till data store.

	När väl ändringar har skickats till data store måste vi ändra state hos DataRow till Unchagned, vilket indikerar att DataRow-objekt är synkroniserade med data store. Vi använder AccewptChanges) för detta.

	Notera att executing AcceptChagnes() även gör att DAtaRow-bojekts Current data row version kopieras till DataRow-objektets Original-version.

	NOTERA: Glöm inte anroa AcceptChanges()
	Kom ihåg att anropa AcceptChanges() efter vi har gjort en lyckad uppdatering till databaseservern eftersom AcceptChagnes() markerar alla rader som Unchagned. Varför?
	Föra tt rader nu är synkade med rader i databasservern.

	RejectChanges() rullar tillbaka DataTable content tillbaka till där den
	var vid skapadet eller när vi senast ekörde AcceptChanges().

	NOTERA: AccpetChagnes() och RejectChanges() valigtvis återställer RowState till
	Unchagned, men RejectChanges även kopierar DataRow objektets Original data rad version till DataRow-objektets Current data row version.


	Använda SetAdded() och SetModified för att ändra RowSTate:

	DataRow har SetAdded() och SetModified(); enablar en data row state att sättas med forcibly till
	Added eller Modfieid. Dessa operaitioner är användbara när vi vill tvinga en datarad att lagras i en datastore på  ett annat sätt än den data store
	från vilket data ursrunglignen laddades.

	Exempelvis, om vi laddar en rad från en data store och vill skcika ddenna rad till en annan data store, kör vi SetAdded() för att få draden att likna en ny rad.

	En DataAdapter objekt skickar ändringar till data store. När vi anstluter till destination data store
	ser data adapter att  vår rad har row state Added, data adapter objektet kör edå en insert sats för att lägga till raden i destination data store.

	Dessa metodern kan exekveras enbart på DataRowobjekt vars row state är Unchanged. Ett försöka tt köra dessa metoder på ett DataRow-objekt med en annan row state kastar ett InvalidOperationException.

	Om SetAdded() körs kastar DataRow-objektet dess Original data rad version då DataRow objekt som har en row state Added aldrig innehåller en Original data rad version.

	Om SetModfiied() anropas ändras DataRow-objektets RowState till Modfiied utan att modifiera Original eller Current dat a row version.

	Radera Data Row, och Undelete
	DataRow har Delete() där row state sätts för raden till Deleted. DataRow-bojekt som har row state Deleted indikerar rader som behöver raderas från data store. När DataRow-objektet raderas kastas Current och Proposed data rad versioner, men Original data row version ligger kvar.

	Ibland behöver vi recover en raderar datarad. DataRow har ingen Undelete(). Vi kan dock avnända RejectChanges() för att rulla tillbkaa tidigare state när den raderade raden fortfarande var där.

	Notera dock att om vi kör RejectChanges() kopierars Original data rad version till Current data rad version. Detta återståller DataRow-objektet till dess state första gången AcceptChagnes() kördes, men alla efterföljande ändringar som gjordes tpoå datat innan  raderingen har been discarded.

	Enumerera Data Tabellen.

	Det är möjligt att loopa genom rader och kolumner av data tabellen genom att avnända foreach. Följande kod visar hur rader och kolumner hos data table kan enumereras:

	public void EnumerateTable(DataTable cars)
	{
		var buffer = new System.Text.StringBuilder();
	
		foreach (DataColumn dc in cars.Columns)
		{
			buffer.AppendFormat("{0,15} ", dc.ColumnName);
		}
	
		buffer.Append("\r\n");
	
		foreach(DataRopw dr in cars.Rows) 
		{
			if(dr.RowState == DataRowState.Deleted)
			{
				buffer.Append("Deleted Row");
			}
			else
			{
				foreach (DataColumn dc in cars.Columns)
				{
					buffer.AppendFormat("{0,15} ", dr[dc]);
				}
			}
			buffer.Append("\r\n");
		}
		textBox1.Text = buffer.ToString();
	}

				// Finns även Load() som kan användas för att uppdatera existerade DataRow-objekt eller ladda ett nytt DataRow-objekt.
	
		static void DataSet()
			{
				// Vi kan skapa DataSet programmatiskt eller genom att förse med en XML Schema (XSD) definition.
				// Exempel på hur vi skapar programmatiskt:
				DataSet dhss = new DataSet("DHSS");
		/*
			
	DataSet vendorData = new DataSet("VEndorData");

	DataTable vendors = vendorData.Tables.Add("Vendors");
	vendors.Columns.Add("Id", typeof(Guid));
	vendors.Columns.Add("Name", typeof(string));
	vendors.Columns.Add("Address1", typeof(string));
	vendors.Columns.Add("Address2", typeof(string));
	vendors.Columns.Add("City", typeof(string));
	vendors.Columns.Add("STate", typeof(string));
	vendors.Columns.Add("ZipCode", typeof(string));
	vendors.Columns.Add("Country", typeof(string));
	vendors.PrimeryKey = new DataColumn() { parts.Columns["Id"] };

	DataTable parts = vendorData.Tables.ADd("Parts");
	parts.Columns.Add("Id", typeof(Guid));
	parts.Columns.Add("VendorId", typeof(Guid));
	parts.Columns.ADd("PartCode", typeof(string));
	parts.Columns.ADd("PartDescirption", typeof(string));
	parts.Columns.ADd("Cost", typeof(decimal));
	parts.Columns.ADd("RetailPrice", tyupeof(decimal));
	parts.PrimertyKey = new DataColumn[] { parts.Columns["Id"] };

	vendorData.Relations.Add("vendors_parts", vendors.Columns["Id"], parts.Columns["VendorId"]);



	// Accessing data table för vendors:
	DataTable vendorTable = vendorData.Tables["Vendors"];

	Vad händer om tabellnamnet är felstavat? Ett untantag kastas vid runtime (notera, vid runtime).
	ETt bättre tillvägagångssätt är att skapa en ny, specializerad DataSet-klass som ärver
	från DataSet, där vi lägger till en egenskap för varje tabell. Exempelvis, en specialiserad DataSet-klass
	kan innehålla en egenskap kallad Vendors som kan accessas på följande vis:

	DataTAble vendorTable = vendorData.Vendors;

	Om vi använder denna syntaxen kommer en compilte time error genereras om Vendor inte stavas korrekt.
	Dessutom reducerar risken aför felstavning eftersom VS intellisens visar Vendors egenskapen för snabb selection när raden av kod håller
	på att skrivas.

	Standard DataSet-klasen är en otyupad data set, och den specialiserade är en typad data set.

	Vi kan skapa en typad DataSet klass manuellt, men det är vanligtvis bättre att göra detta via XSD som genererar typad DAtaSet klass.

	VS innehåller ett verktyg kallad DataSet Editor som vi kan använda för att skapa och modifiera en XSD-fil grafiskt vilket i sin tur kan generera typad DataSet klass.

	Vi kan anropa DataSet Editor genom att lägga till en DataSet fil till ett VS proejtk: högerklicka projektet, välj ADd, välj New ITem och välj DaaSet template i Data-sektionen.

	Efter vi har lagt till DataSet templaten till projektet kommer denna öppas för oss att redigera genom att anvädna SDataSet Editor.

	Notera att det kan vara så att v i måste trycka Show All Files-knappen för att kunna se filen.

	*/
}