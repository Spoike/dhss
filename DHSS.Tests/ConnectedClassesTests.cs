﻿using System.Configuration;
using System.Data.SqlClient;
using NUnit.Framework;

namespace DHSS.Tests
{
	[TestFixture]
	public class ConnectedClassesTests
	{
		
		public void CreatingConnection()
		{
			/*
			// DbConnection är en abstrakt klass som varje provider ärver från. För att kunna upprätta en anslutning till en datakälla
			// behöver man ange en giltig connection string.

			// Vi börjar med ett en instans av DbConnection.
			var connection = new SqlConnection();

			string connectionString;

			// Nedan är exampel på connection strings.

			// Server=. indikeraratt vi vill gå mot default instans på vår lokala maskin. Vi hade även kunnat skriva localhost eller (local).
			// Trusted Connection innebär att vi vill använda vårt Windows inloggningskonto för att autentisera med SQL Server, dvs. säkerheten 
			// baserat på nuvarande inloggad användare.
			connectionString = "Server=.;Database=Northwind;Trusted_Connection=true";

			// Vi har angivit en timeout på 30 sekunder, vilket innebär att ett undantag kommer kastas om vi väntat i 30 sekunder
			// på att upprätta en anslutning. Vi have även specificerat att säkerhetsinformation inte ska persisteras.
			connectionString = "Server=.;Database=Northwind;Integrated Security=SSPI;Persist Security Info=False;Connection Timeout=30";

			// Här har vi angivit att vi ska gå mot en lokal instans av SQL Express och vi pekar ut var MDF-filen finns. User Instance=True gör
			// att en instans av SQL Express startar, som nuvarande användares konto. Notera att vi även kan attacha en SQL Server databasfil, men
			// User Instance=True fungerar inte för denna. Dessutom håller SQL Server databasen attachad även när applikationen avslutar, så nästa
			// gång vi kör SQL Server kommer ett undantag att kastas eftersom datafilen redan är attachad.
			connectionString = "Data Source=.\SQLEXPRESS;AttachDBFilename=C:\Temp\Northwind.MDF;Integrated Security=True;User Instance=True";

			// Fråga: Måste loggfilen existera för att kunna komma åt SQL Express-instansen? Northwind_LOG.LDF.
			// Fråga: Vad innebär Integrated Security=True inom detta kontextet?
			// Fråga: Vad innebär User Instance=True

			// AttachDBFile kan även förstå nyckelordet "DataDictionary", för att använda applikationens data dictionary.
			connectionString = "Data Source=.\SQLEXPRESS;AttachDBFilename=|DataDictionary|Northwind.MDF;Integrated Security=True;User Instance=True";

			// Hur resolvas DataDictionary? Internt innehåller System.Data.dll en klass kallad System.Data.Common.DbConnectionOptions, vilken
			// har en metod som kallas ExpandDataDictionary. Denna inkluderar kod för att resolva |DataDictionary|.
			// Data dictionary sätts för varje applikation som är installeard på en lokal dator. Dess plats ligger i användarens 
			// Documents And Settings-katalog.
		
			// BaseDirectory av CurrentDomain kommer användas om det inte finns någon data dictionary. BaseDirectory innehåller den
			// kompilerade applikationen. När vi utvecklar är detta bin\Debug i projektmappen. Istället för att placera databasefilen
			// direkt i den kompilerade applikationsfoldern är det bättre att placera den i projektfoldern. I Solution Explorer, klicka
			// på filen och i properties-fönstret, sätt "Copy to output directory" till "Copy always" eller "Copy if Newer".

			// Kolla upp: En applikations databasfil (.mdf) och dess loggfil (.ldf) är inkluderade vid installationstillfället och 
			// markerade som en "data"-fil; kopieras till denna katalog när applikationen installeras. Om applikationen avinstalleras
			// kommer applikationens data-folder och innehållet förstöras.

			// ??? Vad innebär Integrated Security=True och User Instance=True i detta sammanhanget?

			// När vi är klara med anslutningen måste vi stänga den. Detta kan vi göra på ett par olika sätt: anropa Close() eller
			// använd oss av ett using()-block.

			// Vi kan lagra connection strings i konfigurationsfil, vilklet låter oss ändra dessa utan att behöva kompilera om applikationen.
			// Brukar placeras i App.config eller Web.config, beroende på om det är en webbapplikation eller ej.

			// Se App.config.

			// Vi kan hämta connection strings programmatiskt. Behöver lägga till assembly System.Configuration.
			connectionString = ConfigurationManager.ConnectionStrings["nw"].ConnectionString;
			
			// Vi kan komma åt inställningarna från connection string.
			var name = ConfigurationManager.ConnectionStrings["nw"].Name;
			var provider = ConfigurationManager.ConnectionStrings["nw"].ProviderName;

			// Vi testar öppna en anslutning.
			connection.ConnectionString = connectionString;
			connection.Open();



			connection.Close();

			// Eftersom Connection-klassen implementerar gränssnittet IDisposable kan vi definiera en anslutning i ett using()-block:
			using(var conn = new SqlConnection(connectionString))
			{

				// Vi behöver inte stänga anslutningen här, det sker automatiskt när vi går ur blocket. När using()-blocket avslutas
				// kommer Dispose() anropas på Connection-objektet, vilket i sin tur anropar Close(). Best practice är att använda 
				// ett using()-block. Skulle ett undantag kastas inuti ett using()-block kommer anslutningen stängas automatiskt.

				// NOTERA: Om vi behöver lämna uppkopplingen öppen mellan flera olika metodanrop kan vi inte använda oss av using().
			}
			
			// Kryptera kommunikationen med SQL Server

			// För att slå på kryptering av kommunikation mellan klient och SQL Server måste ett digitalt certificat installeras
			// på SQL Server. Efter detta kan vi lägga till "Encrypted=True" i connection string för att slå på kryptering.
			connectionString = "Data Source=.\\SQLEXPRESS;AttachDBFilename=C:\\Temp\\Northwind.mdf;Integrated Security=True;User Instance=True;Encrypt=True";

			// Notera att vi använder dubbla \\ ovan. Detta måste vi göra för att escapa \. Ett alternativ är att prependa strängen med @, vilket
			// slår av escape bearbetning.
			connectionString = @"Data Source=.\SQLEXPRESS;AttachDBFilename=C:\Temp\Northwind.mdf;Integrated Security=True;User Instance=True;Encrypt=True";

			// När det gäller webbapplikationer är det viktigt att man krypterar connection strings. Detta kan göras med aspnet_regiis.exe.
			// Det vi gör är att vi krypterar innehållet av Web.config. Kryptering och dekryptering av Web.config görs med 
			// System.Configuration.DPAPIProtectedConfigurationProvider, vilken använder Windows Data Protection API:et (vilken använder RSA
			// krypteringsalgoritm för att kryptera och dekryptera data).

			// När vi använder samma krypterad konfigurationsfil på flera datorer i en webfarm låter enbart
			// System.Configuration.RSAProtectedConfigurationProvider oss exportera krypteringsnycklarna som krypterade data, och importera
			// dom till en annan server. Detta är default inställning.

			// Kryptera connectionStrings-sektionen genom att köra:
			// > aspnet_regiis -pef "connectionStrings" "C:\path\to\our\website"

			// Notera att switchen -pef kräver att vi skickar in sökvägen till websajten. Om vi nu lägger till eller ändrar i connectionStrings-sektionen
			// i konfigurationsfilen behöver vi inte köra aspnet_regiis igen, detta fixas automatiskt.

			// Dekryptera connectionStrings-sektionen genom att köra:
			// > aspnet_regiis -pdf "connectionStrings" "C:\path\to\our\website"

			*/



		}

	/*	
		
Intnernt inehåller System.Data.dll en klass kallad System.Data.Common.DbConnectionOptions, vilket har en metod kallad
ExpandDataDictroinary. DEnna inkluderar kod för att resolva |DataDictionary|:

var path = (string) AppDomain.CurrentDomain.GetData("DataDicationary"));
if (string.IsNullOrEmpty(path))
{
	path = AppDomain.CurrentDomain.BaseDictionary;
}
return path;
	




			*/





		

			/*

För ett exekvera kommando mot databasen behöver vi ha en öppen anslutning + ett command-objekt.

Vi använder DbCommand för att skicka SQL till databased; kan vara DML (Data Manipulation Language. och DDL - Data Defintion Language, som vi skickar).  Vi kan alltså skapa tabeller och modifiera schema information.

Ett DbConnection objekt kan skickas in i DbComand konstruktor eller attachas till ett DbComannd-objekt. Connecdtion property efter DbComnad har skapats.; bästa där dock att använda createcommand hos DbConnection så att provider-specifik kod b3gränsas till skapandet av DbConnection objekt, och DbConnection-objekt automatiskt skapar lämplig provider-specifik kommando oobjket behind the scenene.

DbCommand also requires a valid value for its CommandText and CommandType properties. Följndade kod visar hur skapa och initilera ett DbCommand-objekt.


var nw = ConfigurationManager.ConnectionString["nw"];
var connection = new SqlConnection(nw.ConnectionString);
var cmd = connection.CreateCommand();

cmd.CommandType = CommandType.StoredProcedure;
// namnet på stored procedure
cmd.CommandText = "CustOrderHist";

!! glöm inte stänga connection !!


Ofta behöver vi skicka in parameter till sorted procedures:

var nw = ConfigurationManger.ConnectionStrings["nw"];
var connection = new SqlConnection();
connection.ConnectionString = nw.ConnectionString;
var cmd = connection.CreateCommand();
cmd.CommandType = CommandType.StoredProcedure;
cmd.CommandText = "CustOrderHist";
DbParameter parm = cmd.CreateParameter();
parm.ParameterName = "@Id";
parm.Value = "ANATR";
cmd.Parameters.Add(parm);

NOTE: Parameters are different based on the provider

The SQL provider requries the parameter names to match the parameter names defined in the stored procedure.
The creation of the parameters is therefore not order dependent.

The OleDb provider, however, requires the paramters to be defined in the same order as they are in the stored
procedure. This means the name assigned to the parameter need tnot match the name defined in the stored procedure.



// Retrieve the value currently in the @Id SQL parameter:

var id = (string) cmd.Parameters["@Id"].Value;



:: ExecuteNonQuery()

Kör ExecuteNonQuery() när vi inte förväntar os att kommandot ska returnera några rader - en insert, update, eller delete query esempelvis. Metoden returnerar ett heltal som representerar antal rader sompåverkade av operationen.

private void menuExecuteNonQuery_Click(object sender, EventArgs e)
{
	var nw = ConfigurationManager.ConnectionStrings["nw"];
	int count = 0;
	using (var connection = new SqlConnection())
	{
		connection.ConnectionString = nw.ConnectionString;
		var cmd = connection.CreateCommand();
		cmd.CommandType = CommandType.Text
		cmd.CommandText = "UPDATE Products Set UnitPrice = UnitPrice * 1.1 WHERE ProuctID = ..."
		connection.Open(); // Notera att vi måste öppna anslutningen.
		count = cmd.ExecuteNonQuery();
	}
	MessageBox.Show(count.ToString());
}

:: ExecuteReader()

- returnerar en DbDataReader-instans; ett forward-only read-only service side cursor.

DbDataReader objekt kan skapas enbart genom att exekvera en av ExecuteReaders metoder på DbCommand-objektet. Följande
exempel använder ExecuteReder för att skapa en DbDataReader objekt med the selection results and then continlusly loops trough the results
unilt the end fo data has been reached (when the Read methods returns false).

var nw = ConfigurationManager.ConnectionStrings["nw"];
var connection = new SqlConnection();
connection.ConnectionString = nw.ConnectionString;
var cmd = connection.CreateCommand();
cmd.CommandType = CommandType.Text;
cmd.CommandText = "SELECT ProductID, UnitPrice FROM Products";
conneciton.Open();
DbDataReader rdr = cmd.ExecuteReader();
while (rdr.Read())
{
	MessageBox.Show(rdr["ProductID"]);
}
connection.Close();

:: ExecuteScalar()

- när vi returnerar enbart ett värde, exe

SELECT COUNT(*) FROM Products

Om vi använder ExecuteScalar() kommer .NET Framework run time not incure the overhead to product objects that read the 
result stream, which means less resources usage and better performancde. The following code shows how ot use the ExecuteScalar
method to easily retreive the number of rows in the Sales table directly into a varaible called count:

var nw = ConfigurationManager.ConnectionStrings["nw"];
var connection = new SqlConnection();
connection.ConnectionString = nw.ConnectionString;
var cmd = connection.CreateCommand();
cmd.CommandTyupe = CommandType.Text;
cmd.CommandText = "SELECT COUNT(*) FROM Products";
connecdtion.Open();

int count = (int) cmd.ExecuteScalar();
connection.Close();

If you can use this approach, you should.

:: DbDataReader objekt

- förser med hög prestnada metod att hämta data från data store
- ger oss forward-only, read-only, server side cursor
- ideal för att populare ListBox-objekt och DropDownList objekt

Dock inget bra alternativ om behöver utföra operationer mocch modifiera data och skicka tillbaka ändringarna till databasen.

För datamodifirationer, använd DbDataAdapter.

Read() hos DbDataReader läser in data i dess buffer. Enbart en rad data är tillgänglig vid varje given tidpunkt, vilket
innebär att all data från databasen inte behöver läsas in i applikioanten innan den bearbetas.

var nw = ConfigurationManager.ConnectionStrings["nw"];
var connection = new SqlConnection();
connection.ConnectionString = nw.ConnectionString;
var cmd = connection.CreateCommand();
cmd.CommandType = CommandType.Text;
cmd.CommandText = "SELECT ProductID, ProductName FROM Products";
connecditon.Open();
var rdr = cmd.ExecuteReader();
var products = new DataTAble();
products.Load(rdr, LoadOption.Upsert);
connection.Close();
cmdProducts.DataSource = products;
cmdProducts.DisplayMember = "ProductName";
cmdProducts.ValueMember = "ProductID";

DataTable-objektets Load() har en LoadOption parameter som ger oss the opation to deciding which DataRowVersion objekct should get the incoming data. For example, if you load a dDataTAble objekt, modifiy the data, and the n save the changes back to the database, you might encoutner concurrency errors if someone else has modified the databbetween the time you got the data and th etime you attemtped to save the data. One option is to load the DataTable objekt again, using the defalt PReserveCurrentValues enumeration value, which loads the original DataRowVersion objekt with the data from the database while leaving the current DataRowVersion objekt untouched. Next you can simply execute the Update method again, and the database will be updated successfully.

For this to work propertly, the DataTable objket must have a predefined primary key. Failure to define a primary key results in duplicate DataRow objekts being added to the DataTable object. The LoadOption enumeration members are described below:

LoadOption enumeration members

LOADOPTION MEMBER			BESRKIVNING

OverwriteChanges			Skriver över original och current DataRowVersion objekt och ändrar row state till Unchagned. Nya rader kommer ha row state Unchagned as well

PReserveChanges (default)	Skriver över original DataRowVersion objekt men modifierar inte current DataRowVersion objekt. Nya rader kommer ha row state Unchagned as well


Upsert						Skriver över current DataRowVErsion objekt men modifierar inte original dDataRowVersion objekt. Nya
rader kommer ha row state Added. Rader som har en rad state of Unchagned will have a row state of Unchaged if the current DAtaRowVErsion objekt is the same as the original one, but if they are different, the row state will be Modfied.


:: Använda Multipla Acdtive Results Sets (MARS) för att exekvera muiltipla kommando på en Connection

DbDataReader objekt är ett av de snabbaste sätten att hämta data från databasen, men problemet är att den håller
en öppen server-side cursor medans vi loopar över resultatet av querin. Om vi försker köra en annan kommando 
medans första fortfarande exevkerar kommer vi får en InvalidOperationException som säger "Tehre is already an open DataReader
Associated with this Connection which must be closed first". You can avoid this exception by setting the MultipleActiveResultSets
connection string option till true when connecting to Multiple Active Results Sets (MARS)-enabled hosts så som SQL Server 2005 och senare.

Exempelvis, följande connecton sträng visar hur denna inställning läggs till en ny connection string:


<connectionStrings>
	<clear />
	...
	<add name="nwMars"
		providerName="System.Data.SqlClient"
		connectionString="Data Source=.\SQLEXPRESS;
		attachDbFilename=|DataDirectory|Northwind.MDF;
		Integrated Security=True;
		User Instance=True;
		MultipleActiveResultSets=True" />		
</connectionStrings>

MARS does not provide any performance gains, but it does simpliy your coding efforts.
Think of a scenario in which you execute a query to get a list of stores, and, while you loop through
a list of stores that are returend, you want to execute a second query to get the total quantty of books sold.

NOTE: Be careful about using MARS

MARS is not performant; it is a feature that can simplify the code while minimizing the quantity of connections to the server.

MARS är inte något vi inte kan leva utan; gör helt enkelt bara vår programmering enklare. Faktum är att sätter vi MulipleActiveResultSet till true kommer detta har en negativ performance impact, så detta är inget vi bara ska slå på
på måfå.

På en databasserver utan MARS, you could first collect the list fo stores into a collection and close the connection. After that you can loop through the collection
to get each store ID and execute a query to get the total quantity of books sold for ht atstore. This means that you loop through the stores twice, once to populate the collection and again to get each stores and execute a query to get the stores quantity of book sales.

Another solution (and probably the best solution) is simply to create two connections: one for the store list and one for the quantity of books-sold query.

Another benefit MARS provides is that you might have purchased database client licenes based on the quantity of connections to the database. Without MARS, you would have to open a separate connection to the db for each ccommand that needs to run at the same time, which means that you might need to purchase more client license.

Theh following code sample shows how MARS can perform the nested quaries, using the Customers table to retrieve the count of Orders placed for each customer.
private void menuMars_Click(object sender, EventArgs e)
{
	var nw = ConfigurationManager.ConnectionStrings["nw"];
	using (var connection = new SqlConnection())
	{
		connectoin.ConnectionString = new SqlConnection();
		var cmd = connection.CreateCommand();
		cmd.CommandType = CommandType.Text;
		cmd.CommandText = "SELECT CustomerID, CompnayName FROM Customers";
		connection.Open();
		var rdr = cmd.ExecuteReader();
		while(rdr.Read())
		{
			var ordersCmd = connection.CreateCommand();
			ordersCmd.CommandType = CommandType.Text;
			ordersCmd.CommandText = "SELECT COUNT(OrderID) FROM Orders WHERE (CustomerID = @CustId)";
			var parm = ordersCmd.CreateParameter();
			parm.ParameterName = "@CustId";
			parm.Value = rdr["CustomerID"];
			ordersCmd.Parameters.Add(parm);
			var qtyOrders = ordersCmd.ExecuteScalar();
			MessageBox.Show(rdr["CompanyName"].ToString());
		}
	}
}

:: Performing Bulk Copy operations with a SqlBulkCopy object

Ofta behöver man kopiera stora mängder data från en plats till en annan.

SqlBulkCopy-klassen ger oss en högprestanda metod för att kopiera data till en tabell i en SQL SErver database.

The source of the copy is constrainted by the overloads of the WriteToServer() which can accept
an array of DataRow objects, an objet that implements the IDataReader interafce, a DataTAble object,
or DataTable and DataRowState. This variety of params means we can retreive data from most locations.

Vi kan plocka data från annan db server eller XML dokument, till en SQL Server-tabell.

var nw = ConfigurationManger.ConnectionStrings["nw"];
var nwconnection = new SqlConnection();
nwConnection.ConnectionString = nw.ConnectionSTring;

var bulkCopy = ConfigurationManager.ConnectionStrings["BulkCopy"];
var bulkConnection = new SqlConnection();
bulkConnection.ConnectionString = bulkCpopy.ConnectionSTring;
var cmd = nwConnection.CreateCommand();
cmd.CommandType = CommandType.Text;
cmd.CommandText = "SELECT CustomerID, CompanyName FROM Customers";
nwConnection.Open();
bulkConnection.Open();
var rdr = cmd.ExecuteReader();
var bc = new SqlBulkCopy(bulkConnection);
bc.DestinationTableName = "CustomerList";
bc.WriteToServer(rdr);
nwConnection.Close();
bulkConnection.Close();

Consider using the IDataReader overload whenever possible to get the best performance using the least resources.
You can decide how much data should be copied baseed on the query you use. For example, the preceding code sample retrieved only the customer names and IDs. The retrieved dataa could be further limited by adding a WHERE clause to the SQL statement.

:: DbDataAdapter object

Vi använder DbDataAdapter för att hämta och uppdatera data mellan en data table aoch en data store.

DbDataAdapter har property SelectCommand som vi använder när vi hämtar data.
SelectCommand måste innehålla en giltig DbCommand objekt vilket i sin tur måste ha en giltig conneciton.

Internet har SelectCommand en ExecuteReader-metod, vilken exekveras för att få en DbDataReader objekt för att populera en DataTable obekt.

DbDataAdapter har även 
	- InsertCommand
	- UpdateCommand
	- DeleteCommand
	
	Vi behöver inte skapa dessa om vi bara tänker läsa från data store, men om vi skapar en av dess tre måste vi även skapa alla andra (dvs. select, insert, update, delete). (notera att om vi bara skapar select behöver vi inte skapa de andra!).
	
	När DbDataAdapter används för at hämta eller upddatera data undersöker den status hos connection. Om connection är öppen använder DbDataAdapter den öppna anslutningen och lämnar uppkopplingen öppen. Om uppkopplingen är stängde öppnar DbDataAdapter uppkopplingen, använder den och stänger den sedan automatiskt.
	
	Om vi aldrig öppnar anslutgenin behöver vi alltså inte stänga den själv. However, if you have many data adapters that will be used in one oeration you can get better peformance by manyally opening the connection bfore you call all the data adapters, ; just be sure to close the connection when you're done.
	
:: Använda Fill()

Fill() flyttar data från data store till DataTable-objekt som vi skickar in i metoden.Finns flera overloads av denna; om t.ex. skickar in ett tomt DataSet kommer detta skapa upp et DataTable automatiskt om ett source DAtaTable inte anges.

var nw = ConfigurationManger.ConnectionStrings["nw"];
var nwconnection = new SqlConnection();
nwConnection.ConnectionString = nw.ConnectionSTring;

var cmd = (SqlCommand) connection.CreateCommand();
cmd.CommandType = CommandType.Text;
cmd.CommandText = "SELECT CustoemrID, CompanyName FROM Customres";
var da = new SqlDataAdapter(cmd);
var nwSet = new DataSet("nw");
da.Fill(nwSet, "Customers");
MessageBox.Show("DAtaSet Filled");

Många utvecklare försöker använda en enda DbDataAdapter klass för alla queries eller 
försöker använda en enda DbDataAdapter klass för att köra en SQL sats som returenar 
ett resultat set från flera tabeller som är joinade
tillsammanas inom SQL queryn.

Om vi behöver lagra dataändringarna, consider using a separate DbDataAdapter klass frö varje data tabell som laddas. OIOf all you need is a read-only
data table, you can simply use a DbCommand object and DbDataReader ojbect to load the data table.-

:: Spara ändringar till databasen mha Update()

Update() sparar data table modifikationer till dtaabasen genom att ämta ändringar från datatabellen och sedan använda
InsertCommand eller UpdateCommand, eller DeleteComamnd för att skicka lämpling ändringar till databasen på en
rad-för-rad basis.

Update() hämtar DataRow-objekt som har ändrats genom att kolla på RowState egenskapen hos varje rad. Om RowSTate är allt annat
än Unchagned kommer Update() att skicka ändringarna till databasen.

För att Update() ska fungera måste samtliga kommando tilldelats till DbDataAdpater-objektet. Normalt innebär att att vi skapar individuella DbCommand-objekt för varje kommando. Vi kan enkelt skapa kommandona genom att använda DbDataAdpater Configuration Wizard, which stats when a DbDataAdpater objekt is dropped on the Windows form. The wizard can genereate stored prodcuders for all four commands.

Another way to populate the DbDataAdapter objects commands is to use th DbCommandBuilder objekt. This object creates the InsertCommand, Updatecommand, and DeleteCommand properteies as long as a valid SelecdtCommand propety exists.

DbDataAdapter is great for ad hoc changes and demos, but it's generally better to use stored procedures for all database access because, in SQL Server, it's easier to set permissions on stored procedures than it's to set permissions on tables.

The following code demonstrates a siple update to the database, using SqlDataAdapter, which is the SQL Server-specific version of DbDataAdpater. After the changes are savedd, the rsults are displayed on the DataGridView objectd, called DAtaGridView2.

var nw = ConfigurationManger.ConnectionStrings["nw"];
var nwconnection = new SqlConnection();
nwConnection.ConnectionString = nw.ConnectionSTring;

var cmd = connection.CreateCommand();
cmd.CommandType	= CommandType.Text;
cmd.CommandText = "SELECT * FROM Customers";
var da = newn SqlDataAdapter(cmd);
var nwSet = new DataSet("nw");
var bldr = new SqlCommandBuilder(da);
da.Fill(nwSet, "Customers");

/Å/ Modify existing row
var customersTable = nwSet.Tables["Customers"];
var updRow = customersTable.Select("CustomerID='WOLZA'")[0];
updRow["CompanyName"] = "New Wolza Company";

// Add new row
customersTable.Rows.Add("AAAAA", "Five A Company");

// Delete a row, note that you cannot delete a customer who has orders.
var delRow = customersTable.Select("CustomerID='PARIS'")[0];
delRow.Delete();

// Send changes to database.
da.Update(nwSet, "Customers");
dataGridView2.DAtaSource = nwSet;
dataGridView2.DataMember = "Customers";
MessageBox.Show("Update Complete");

NOTE: Don't Execute Twice!

If you exewcute this code twice, an exception will be thwon indicating that you are trying to indsert duplicate rows.
Of you stop runing the app and tehn run it again, you will be abel to execute again, because th eNorthwind db has the
Copåy to Ouytput Directory property set to Coppy Always, which means that you get a new clean copy of the database every time you build the project.

:: Saving changes to the database in batches

Om vi har SQL Profiler verktyget installerat, vilket vi får när vi kör fulla versionen av SQL Server, kan vi 
använda denna för att se update kommando skickade till SQL Server. Vi kommer lägga märke till att individuella inset, update, och delete kommando skickas till SQL Server på rad-per-rad basis.

Ett sätt att öka update prestanda är att skicka ändringar till databasservern i batchas genm att tilldela ett värde till DbDataAdapter-objektks UpdateBatchSize egenskap. DEfault är 1 för denna, vilket gör att vi skickar rad-för-rad.

Sätter vi värdet itll 0 instruerar vi DbDataAdapter ojekttet att skapa största möjligt batch size för ändringar, eller så kan vi sätta
till ett värde vi vill ha för varje batch.

Att sätta UpdateBatchSiuze till ett störra antal än det antal änringar som behöver skickas är detta samma som att sätta till 0.

NOTERA: ATt sätta UpdateBatchSize till 0 är ett snabbt sätt att boosta update prestanda för DbDataAdapter-objekt.

One way to confirm that the changes are being sent to the database server in batches is to register a handler to the RowUpdated event of the DbDataAdapter derived class instances. The event handler method
recevies the number of rrows affected in the last batch. 

When UpdateBatchSize is set to 1 the RecordsAffected property is always 1. 

NOTER: The RowUpdate event finns inte på DbDataAdapter basklassen; finns dock i SqlDataAdapter-klassen.

private SqlDataAdapter da = new SqlDataAdapter();
private System.Text.StringBuilder sb = new System.Text.StringBuilder();

private void rowUpdated(object sender, SqlRowUpdatedEventArgs e)
{
	sb.Append("Rows: " + e.RecrodsAffected.ToString() + "\n\r");
}

private void menuUpdateBatch_Click(object sender, EventArgs e)
{
	// Event subscription is normally placed in constructor but is here to encapsulate the sample.
	da.RowUpdated += rowUpdated;
	var nw = ConfigurationManager.ConnectionStrings["nw"];
	var connection = new SqlConnection();
	connection.ConnectionString = nw.ConnectionString;
	
	var cmd = connection.CreateCommand();
	cmd.CommandType = CommandType.Text;
	cmd.CommandText = "SELECT * FROM Customers";
	da.SelectCommand = cmd;
	var nwSet = new DataSet("nw");
	var bldr = new SqlCommandBuilder(da);
	da.Fill(nwSet, "Customers");
	
	// Modify data here
	foreach (DataRow dr in nwSet.Tables["Customers".Rows])
	{
		dr["CompanyName"] = dr["CompanyName"].ToString().ToUppeR();
	}
	
	sb.Clear();
	da.UpdateBatchSize = 40;
	da.Update(nwSet, "Customers");
	
	// If event subscription is in the constructor, no need to remove it here.
	da.RowUpdated -= rowUpdated;
	MessageBox.Show(sb.ToString());
}

:: DbProviderFactory classes

För att inte behöva använda sig av provider specifika klasser, använd factory


DbProviderFactory factory = SqlClientFActory.Instance;

public DbConnection GetProviderConnection
{
	var connection = factory.CreateConnection();
	connection.ConnectionSTring = @"...";
}

We can use the factory variable to create any of the other SQL Server-specific objects.
Note that SqlDataReader is created indirectly by creating a SQL command and then using the ExecuteReader method:

private DataTable GetData(stirng commandText, CommandType commandType)
{
	// Get SqlDbCommand.
	var command = factyory.CreateCommand();
	command.Connection = GetProviderConnection();
	if (command.Connection == null) return null;
	command.CommandText = commandText;
	command.CommandType = commandType;
	command.Connection.Open();
	var dataTable = new DAtaTable();
	
	// Get SqlDataReader and populate data table.
	dataTable.Load(command.ExecuteREader());
	command.Connection.Close();
	return dataTAble;
}

This code sample uses the factory variable to create a DbCommand class, which is then used to create the DbDataReader object.


:: Using DbExceptin to Catch Provider Exceptions

Alla provdier-specifika oundantag äver från en gemensam basklass: DbException.
När vi jobbar med en provider-neutral kodmodell kan vi fånga DbExdeption istället för provider-specifika undantag.

:: Working with SQL Server User-Defined Types (UDTs)

SQL Server 2005 och senare har stöd för hosta .NET Framework common language runtime (SLQCLR), så vi kan använda C# för att skriva kod som kör i SQL Server. En av features som SQLCLR ger oss är förmågan att skapa användar-definierade typer (UDT:er).

Deta kan vara i form av en .NET Framewokr klass eller strutkru med metoder och egensaper.

DTer kan specificeras som koluntyp i en tabelldefinition, de kan vara en variables typ i T-SQL batcher, eller kan vara typen för ett argument till en T-SQL funktion eller lagrad procedur.

Vi kan skriva våra egna UDT:er; Microsoft har dock lagt till egna UDTer till SQL Server. For example, in SQL Server 2008 the new Geography and Geometry types are UDTs.

To get the most benefit from a UDT at the client you will typically want to use a UDT on the client in a typed manner. This requires you to set a reference to the same assembly that contains the UDT registerd in SQL server. This means the assembly must be available to the clienet appliikation, most ommonly eight by copying the assbemly into the same folder as the client executable or by installing the assembly into the GAC (global assembly cache). If you don't need typed access to the UDT, only the raw bytes of the UDT returned from SQL Server, you don't need to set a reerence to the assembly.

This could be used when you know the physical structure of the data, and you're going to parse th edata and assign it to a different object.

The following example code demonstrates theh creation of a table called Locations in SQL SSErver. Before creating thte table, a command is executed to see whether the table already exists. IF the table exists, it's dropped. After that the Locations table is created:

var nw = ConfigurationMangaer.ConnectionStrings["nw"];
var nwConnection = new SqlConnection(nw.ConnectionString);
var cmd = nwConnection.CreateCommand();
cmd.CommandText = "IF EXISTS (SELECT * FROM sys.Tables WHERE Name='Location') DROP TABLE Locations";
nwConnection.Open();
cmd.ExecuteNonQuery();
cmd.CommandText = "CREATE TABLE Locations(ZipCode char(5) PRIMARY KEY NOT NULL, Location Geography)";
cmd.ExecuteNonQuery();
nwConnection.Close();

MessageBox.Show("Location table created");

NOTE: Working with a clean copy of the database
If your Copy To Output Directory settings on your database file are to Copy Always, every time you start the application
you get a new clean database. You will need to re-create the table and then re-populate it. Then you can run other tests.

We see that we use a UDT above (Geography). In order for us to insert a Geography object, we must add a reference to the Microsoft.SqlServer.Types.dll assembly. If we have SQL Server 2008 installed, this asembly is in the GAC, but you can't add
references directly to GAC assemblies. You must locate the assembly, which should be at the following locaiton:

C:\Program Files (x86)\Microsoft SQL Server\100\SDK\Assemblies

This is the location if you have a 64 bit op.

// Add reference to Microsoft.SqlServer.Types.dll
using Microsoft.SqlServer.Types;
using System.Data.SqlTypes;

var nw = ConfigurationManager.ConnectionString["nw"];
var nwConnection = new SqlConnection(nw.ConnectionString);
var cmd = nwConnection.CreateCommand();
var zip = new SqlParameter("@zip", "14710");; // Ashville NY
var loc = new SqlParameter("@loc", SqlDbType.Udt);
loc.UdtTypeName = "geography";
loc.Value = SqlGeography.STGeomFromText(new SqlChars("POINT(42.1018 79.4144"), 4326);
cmd.Parameters.Add(zip);
cmd.Parameters.Add(loc);
nwConnection.Open();
cmd.ExecuteNonQuery();

zip.Value = "44011";
loc.Value = SqlGeography.STGeomFromText(new SqlChars("POINT(41.4484 82.0190), 4326);
cmd.ExecuteNonQuery();

nwConnection.Close();
MessageBox.Show("Entries added");

The number 4326 above is the Geocode for WGS84 round earth model that most people use.

After the rows are inserted into the table we can query for these rows and use them to perform tasks
such as calculating the distance between two locations:

var nw = ConfigurationManager.ConnectionStrings["nw"];
var nwConnection = new SqlConnection(nw.ConnectionString);
var cmd = nwConnection.CreateCommand();
cmd.CommandText = "SELECT Location FROM Locations WHERE ZipCode='14710'";
nwConnection.Open();
var ashvilleNY = (SqlGeography) cmd.ExecuteScalar();
cmd.CommandText = "SELECT Locatoin From Locations WHERE ZipCode='44011'";
var avonOH = (SqlGeography) cmd.ExecuteScalar();
nwConnection.Close();

MessageBox.Show(string.Format("Ashville to Chicago: {0}", ashvilleNY.STDistance(chicagoIL) / 1609.344));  // 1609.344 meters/mile));

Vi ser här att vi kan anropa metoder på UDT-objektet.

Lesson Summary
 - SQL Express is an excellent database server for development because the .mdf database file can be placed into the project and the file can be configured to be copied to the output folder every time the application is built and run
- you use the DbCommand object to send SQL command to a data store. You can also create parameters and pass them to the DbCommand objekt
- The DbDataReader object provides a high-performance method of retrieving data from a data store by delivering a forward-only,
read-only, server-side cursor
- The SqlBulkCopy object can copy data from a number of sources to a SQL Server table
- you can use the DbDataAdapter object to retrieve and update data between a data table and a data store. DbDataAdapter can contain a single SelectCommand property for read-only data, or it can contain SelectCommand, InsertCommand, UpdateCommand and DeleteCommand properties for full updatable data
- The DbProviderFactory object helps you create provider-idenpendet code which might be necessary
when the data store needs to be changed quickly￠￠￠￠ 
			 
			
		}

		static void Transactions()
		{

		}
		
			 
	
:: Transaktioner

Transaktionsfunktionalitet ligger i System.Transactions; detta namespace är inte del av ADO.NET; utvecklades
av Enterprise Services team vid Microsoft.

Vad är en transaktion?
- en atomic unit of work that must be completed in its entirety. Transaktionen lyckas om den commitas, och failar om den abortas.
Transaktioner har 4 essential attribut:

 - atomicity

	Arbetet kan inte delas ner i mindre delar. Även om en transaktion består av t.ex. många SQL-satser, måste dessa
	köras alla eller inga alls. Om ett fel inträffar halvvägs genom måste arbetet återvända till den state den hade 
	innan transaktionen påbörjades

 - consistency

	en transaktion måste jobba på en konsekvent vy av data och måste lämna data i en consistent state. Work in process får
	inte synas för andra transaktioner tills det att transaktioen har committats

 - isolation

	effekterna av andra transaktioner som kör samtigit måste vara osynliga för vår transaktion, och effektiverna av transaktione
	får inte vara synlig för andra pågående transaktioner

 - durability	

	när en transaktion committats måste den persisteras så att den inte förloras pga strömavbrott eller dylikt.
	Enbart committade transaktioner are recovered during power-up and crash recovery; uncommitted work is rolled back
	
ACID - akronym

:: Concurrency Models and Database Locking

Attributen consistency och isolation implementeras med databasens låsningsmekanism, vilket hindrar en transaktion från 
att störa/påverka en annan. Om en transaktion behöver åtkosmt till data som en annan transaktion jobbar med, låses datat
tills dess att första transaktionen är committade eller rullade tillbaka.

Transaktioner som måste accessa låst data tvingas vänta tills låset släpps, vilket innebär att långtköradne transaktioner kan påverka
prestanda och skalbarhet. Anvädning av lås för att förhindra tillgång till data kallas pesimistisk concurrency model.

I en optimistisk concurrency model, används inte lås när data läses. Istället, när uppdateringar görs, kontrolleras data
för att se om denna har förändrats sedan den senast lästste. Om den har det kastas ett undantag och application applices affärslogik för att recover från det.

:: Transaction Isolation Levels

Komplett/hel isolering kan vara wunderbra, men kommer till ett högt pris. Komplett isolering innebär att data som läses eller skrives under en transaktion måste låsas. Ja, även data som läses låses eftersom en query för customer orders should yield samma resultat vid början av transaktionen som vid slutet.

Beroende på vår app kanske vi inte behöver komplett isolering. Genom att tweaka transaktion isolationsniverna kan vi redurea mängden låsning och öka skalbarhet och prestanda. Transaction isloation level påverkar huruivda vi upplever följande:

 - dirty read: att kunna läsa data som har ändrats av en annan transaktion, men som inte committats än. Detta kan vara ett stort problem om transaktionen som ändrade datat rullar tillbaka

 - nonrepeatable read: när en transaktion läser samma rad flera gånger med olika resultat eftersom en annan transaktion har modifierat raden mellan läsningarna

 - phantom read: när en transaktion läser en rad som en annan transaktion kommer radera eller när en andra läsning finner en ny rad som har förts in av en annan transaktion

Lista över transaktionsnivåer tillsammas med deras effekter. Visar även concurrency model för isolation level supports:

LEVEL							DIRTY READ				NONREPETABLE READ			PHANTOM READ			CONCURRENCY MODEL
-------------------------------------------------------------------------------------------------------------------------------
Read Uncommited					Yes						Yes							Yes						None
Read Committed with Locks		No						Yes							Yes						Pessimistic
Read Committed with Snapshots	No						Yes							Yes						Optimistic
Repeatable Read					No						No							Yes						Pessimistic
Snapshot						No						No							No						Optimistic
Serializable					No						No							No						Pessimistic

The following is a description of the concurrenty levels that the table above shows:

Read Uncommited: Queries inuti en transaktion påverkaas av okommittade ändringar i andra transaktioner. Non locks are required, and no locks are honored when data is read.

Read Committed with Locks: default inställning i SQL Server; committade ädnringar är synliga inom andra trasaktioner; långt-körande queries och aggregeringar krävs inte to be point-in-time consistent.

Read Committed with Snapshots: enbart committade uppdateringar är synliga inom en annan transaktion. Ingen lås krävs, och radversioneringer trackar radmodfieringar. Långköradnes queries och aggregates are required to be point-in-time consistent. Denan nivå kommer med the overhead fo the version store. The version store provides increased throughput with reduced locking contention.

Repeatable Read: inom en transaktion är samtliga läsningar consistent; andra transaktioner kan inte påverka vår query resultat eftersom de inte kan avsluta förrän vi har avslutat vår transaktion och släppt våra lås. Denan nivå används primärt när vi läser data med syftet att modifiera data i samma tranasktion.

Snapshot: används när noggranhet krävs för långtkörandes que3aires och multi-statmenet transatkioner men det inte finns någon plan att uppdatera datat. Inga läslåsningar krävs för att förhindra modifiering av andra transatkioner eftersom ändringar inte kommer sees förrän snapshotten avslutar och data modification transaktioner committats. Data kan modifiera inom denna transaktionsnivå at th erisk av konflikter med transaktioner som har uppdaterat samma data efter snappshot transaktionen startade.

Serializable: placeras en range-lås, vilket är en multi-row lås, på the complete row set accessed, vilket förhindrar andra användare från att uppdatera eller föra in rader i data settet tills transaktionen kört klart. Denna data är accurate och consistent through the life of the transaction. This is the most restrictive of the isolation levels. Because of the large amount of locking int his level, you should use it only when necessary.

Version store = retains row version records after the update or delete statment has committed until all active tranactions have committed. The version store essentially retains row version records until all the following transaction types have committed or ended:
 - transactions that are running under Snapshot Isolation
 - transactions that are running under Read Committed with Snapshot Isolation
 - All other transactions that started beforte the current transaction committed

:: Single transactions and distributed transactions

En transaktion är en unit of work som måste utföras med en single durable resource (så som en databas eller en message queue). I .NET Framework representerar en transaktion vanligtvis all det arbetet som kan göras på en enda öppen uppkoppling.

En distribuerad transaktion sträcker sig över flera resurser. I NET Frameowkr, om vi behöver en transaktion som inkluderar arbete över flera uppkopplingar, måste vi utföra en distribuerad transaktion. En distribuerad transaktion använder en 2-fas commit protokoll och en dedikerad transaktions manager. I Windows operativsystem, sedan MS NT, är den dedikerade transaktionshanteraren för att hantera distribuerade transaktioner DTC (Distributed Transaction Coordinator).

:: Skapa en Transaktion

Vi kan skapa två typer av transaktioner:
 - implicit transaktion
 - explit transaktion

Varje SQL-statement kör i sin egen implicita transaktion. Om vi inte uttryckligen skapar en transaktion kommer en skapas implicit åt oss på en statement-by-statment basis. Detta försäkrar att en SQL-sats som uppdaterar många rader antingen kör klart som en enhet eller rullar tillbaka.

En explicit transaktion är något vi själva skapar i programmet. Te.x. nere i databasen med T-SQL:

SET XACT_ABORT ON
BEGIN TRY
	BEGIN TRANSACTION
		-- work here
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION
		-- cleanup code
END CATCH

SQL TRY/CATCH blocket fångar upp fel och rullar tillbaka transaktionen. Koden sätter XACT_ABORT till ON, vilket försäkrar
att samtliga fel under severity nivå 21 hanteras som transaction abort errors. Severity nivå 21 och högre is considered fatala
och stoppar kodexekveringen, vilket även rullar tillbaka transaktionen.

Scopet av transaktioenn är begränsad till satserna inom TRY-bocket, vilket kan inkludera anrop till andra lagrade procedurer.

:: Skapa en transaktion mha ADO.NET DbTransaction

Ett annat sätt att skapa en explicit transaktion är att lägga transaktionsdkoden i .NET Frammwork-kod. DbConnection-objekt har BeginTransaction() vilket skapar ett DbTransaction-objekt:

private void beingTransactionToolStripMenuItem_Click(object sender, EventArgs e)
{

	ConnectionStringSettings cnSettings = ConfigurationManager.ConnectionStrings["nw"];
	
	using(SqlConnection cn = new SqlConnection())
	{
		cn.ConnectionString = cnSetting.ConnectionString;
		
		cn.Open();
		using(SqlTransaction tran = cn.BeginTransaction())
		{
			try
			{
				// work code here
				using (sqlCommand cmd = cn.CreateCommand())
				{
					cmd.Transaction = tran;
					cmd.CommandText = "SELECT count(*) FROM employees";
					int count = (int) cmd.ExecuteScalar();
					MessageBox.Show(count.ToString());
				}
				
				// If we made it this far, commit.
				tran.Commit();
			}
			catch(Exception ex)
			{
				tran.Rollback();
				// cleanup code
				MessageBox.Show(ex.Message);
			}
		}
	}
}

NOTERA: The SqlCommand object must have its Transaction property assigned to the connection's transaction.

The scope of the transaction is limited to the code within the try block, but the transaction was created by a specific
connection object, so the transaction cannot span to a different connection object.

:: Seetting the transaction isolation level

Varje SQL Server connection (SQL session) can have its transaction isolation level set. The settign you assign remains until
the connection is closed or until you assign a new setting. One way you assign the transaction isolation level is to add the 
SQL statement to your stored procesfure. For example, to set the tranasction isolation level to Repeatable Read, add the 
following SQL statement to your stored procedure:

SET TRANSACTION ISOLATION LEVEL REPEATABLE READ

Ett annat sätt att sätta transaktions isolationsnivå är att lägga till en query hint till vår SQL sats. Exempelvis, följande SQL overridar nuvarande sessions isoleringsinställning och använder Read Uncommited isolation nivå för att utföra queryn:

SELECT * FROM CUSTOMERS WITH (NOLOCK)

Transaktionsisoleringsnivån kan även sättas på DbTransaction-klassen, från vilket SqlTransaction ärver. skicka helt enkelt önskad transaktionsisoleringsnivå till BeginTransaction().

:: System.Transactions namespacet

System.Transactions namespace erbjuder enhanched transactional support för att hantera kod och göra det möjligt att hantera transaktioner i a rather simple programming model.

System.Transactions is designed to integrate well with SQL Server 2005 an later and offersr automatic promotionof standrad transactions to fully distributed transactions.

:: Skapa en transaktion genom att använda TransactionScope

Vi kan skapa en transaktion i .NET Framework genom att använda klasser i System.Transactions namespacet.  Vanligaste klassen
är TransactionScope - skapar en standard transaktion kallad en "lokal lättviktstransaktion" som automatiskt promotats till en full-fledged distributed transaktion vid behov.

Denna automatiskt promoted transaktion kallas vanligtvis för en implicit transaktion.

private void systemTransactionToolStripMenuItem_Click(object sender, EventArgs e)
{
	ConnectionStringSettings cnSettings = ConfigurationManager.ConnectionStrings["nw"];
	
	using (TransactionScope ts = new TransactionScope())
	{
		using(SqlConnection cn = new SqlConnection())
		{
		
			cn.ConnectionString = cnSetting.ConnectionString;
			cn.Open();
		
			// work code here
			using(SqlCommand cmd = cn.CreateCommand())
			{
				cmdf.CommandText = "SELECT count(*) FROM employees";
				int count = (int) cmd.ExecuteScalar();
				MessageBox.Show(count.ToString());
			}
		
			// if we made ti this far, commit
			ts.Complete();
		}		
	}
}
 This code starts by creating a TransactionScope object in a using block. If a connection is created, the TransactionScope
object assigns a transaction to this connection so you don't need to add anything to your code to enlist this connection
into the transaction.

The SqlCommand object doesn't need to have the Transaction property assigned, but the SqlCommand object joints the transaction.

If an exception is thrown within the last line of the TransactionScope object's using block, the transaction aborts, and all work is rolled back. The last line of the TransactionScope objects using block calls the Complete() to commit the transatction. This method sets an internal Bolean flag called complete. Theh Complete() can be called donly once. This is a design decision that ensure that you won't continue adding code after a call to Complete and then try calling Complete again. A secon dcall to coComplete will throw an InvalidOperationException.

EXAM TIP: You need to call the Complete() or your transaction will be rolled back.

The scope of the transaction is limited to the code within the TransactionScope object's using block, which includes any and all connections created within the block, even if the connections are created in methods called within the block.
You can see that the Transaction´Scope object offers more functionality than ADO.NET transactions and is easy to code.

NOTE: No difference in performance

The code in this example performs as well as the previous examples showing ADO.NET transactions, so you should consider standardizing your code to use this programing model whenever you need trasactional behavior, ás long as you are using SQL Server 2005 or later. If you are using SQL Server 2000 or different database product, you should continue to use the existing ADO.NET transaction because SQL Server 2000 and other databas products don't know how to create a local lightwiehgt transaction.

:: Setting the transaction options

Vi kan sätta isoleringsnivå och transaktions-timeout på TransactionScope-objektet genom att skapa ett TransactionOptions-objekt. TransactionOptions strukturen har en IsolationLevel-property vi kan använda för att deviate from the default isoleringsnivå of Serializable och employ another isoleringsnivå (så som t.ex. Read Committed). Isoleringsnivån är enbart ett förslag (hint) till databasen. De flesta db motorer försöker används förslagna nivå om möjligt.

TransactionOptions typen have även en Timeout-egenskap som kan användas för att deviate from default som är 1 min.

TransactionScope-objektets konstruktor tar även en TransactionScopeOption enumerationsparameter. Denna kan sättas till ett av följande:

- Required

	Om vår applikation redan har startat en transaktion kommer detta TransactionScope-objekt att joina. Om det inte redan
		finns en pågående transaktion, kommer en ny transaktion att skapas. För att hjlpa förstå fördelarna med denna
		inställningen, tänk dig ett BAckAccount-klass som har Deposit(), Withdraw() och Transfer().
		
		Vi vill skapa ett transaktionsscope i alla tre metoder, men när vi kommer till Transfer() kallar den withdraw och despotmetoderna som redan har en transaktionsscope definierat. Deposit() och Withdraw() kommer alltså nästlas inuti TranseR().
		
		Vi kan inte ta bort TransactionScope från Withdraw() och Deposit() eftersom vi behöver the ability att anropa dessa metoder direkt, och vi vill att som ska exevkera inom en transaktion. Med Required-inställningen kommer Withdraw() och Deposit() att joina Transfer()-tranaktionen. Detta är default inställning.
	

- RequiresNew

	Denna isbtällning startar alltid en ny transaktion, även om det finns en pågånde transaktion. Exepelvis, vi kanske vill att
	audit ska skrivas, oavsett om en tranasktion committar eller rullar tillbaka. Då kan detta inte ligga i transaktionen, utan måste ligga i sin egen.

- Suppress

This setting supresses any activity in this block. IT provides a way to do nontransaction work while there is an ongoing
transaction. Theh execution of the Complete() method is not required in this block and has no effect.

The following code creates and configures a TransactionOptions object, which is padssed to the constructor of the TransactionScope object.

private void transactionScopeOptionsToolStripMenuItem_Click(object sender, EventArgs e)
{
	ConnectionStringSettings cnSetting = ConfiurationManager.ConnectionStrings["nw"];
	TransactionOptions opt = new TransactionOptions();
	opt.IsolationLevel = System.Transactions.IsolationLevel.Serializable;
	using(TransactionScope ts = new TransactionScope(TransactionScopeOption.Required, opt))
	{
		using(SqlConnection cn = new SqlConnection())
		{
			cn.ConnectionString = cnSetting.ConnectionString;
			cn.Open();
			
			// work code here
			
			using (SqlCommand cmd = cn.CreateCommand())
			{
				cmd.CommandText = "SELECT count(*) FROM employees";
				int count = (int) cmd.ExecuteScalar();
				MessageBox.Show(count.ToString());
				
			}
		}
		// kif we made it this far, commit
		ts.Complete();
	}

}

:: Working with distributed Trnsactions

System.Transactions namespace inkluderar Ligthweight Transaction MAnager (LTM), utöver DTC. Använd LTM för att
hantera en single transaction till en durable resurs manager, så som SQL Server 2005 och senare. Volatile resource managers,
which are memory based, can also be enlisted in a signle transaction. The transaction managers are intended to be invisible to the developer, who never needs to write code to access them.

Vi kan enkelt skapar  en distribuerad transaction, genom att avnända TransactionScope och samma programmeingsmodell som användes för single transactions. När vi accessar vår första durable resource manager, skapas en lightweight committable transaktion för att stöda single transaktioner. När vi accessar en andra durable resource manager promotas transaktionen till en distributerad transaktion. När en distribuerad transaktion körs hanterar DTC:n 2-committs protokollet för att committa eller rulla tillbaka transaktionen.

LTM och DTC representerar deras transaktion genom att använda System.Transactions.Transaction-klassen, vilket har en statisk egenskap kallad Current som ger oss åtkomst till nuvarande transaktion. Nuvarande transaktion är känd som the ambient transaction. Denna egenskap är null om det inte finns någon pågående transaktion. Vi kan accessa Current egenskap direkt för att ändra transaktion isolation leel, rulla tillbaka transaktionen, eller titta på transaktionens status.

:: Promotion Detaljer

När en transaktion först skapas försöker den alltid var en lightweight committable transaction, som hanteras av LTM. LTM tillåter underliggande durable resurshanterasr, så som SQL Server 2005 och senare, att hantera transaktionen. Det enda som LTM gör är monitorerar transaktionen for a need to be promoted.

Om promotion krävs kommer LTM:En  instruera the durable resurshanteraren att förse med ett objekt som är kapabelt att utföra en distibuerad transaktion. To support the notification, the durable resource manager must implement the IPromotableSinglePhaseNotification interface. Detta interface, och dess förälder interface, ITransactionPromoter, visas här:

En transaktion kan promotas från en lightweight committable transaktion till en distribuerad transaktion i följande tre scenario:

- when a durable resource manager is used that doesn't implement the IPromotableSinglePhaseNotification interface, så som SQL Server 2000
- när två durable resourse managers are enlisted in the same transaction
- whent the transaction spans multiple application domains

: :Viewing distributed transaktioner

DTC:en är tillgänglig via Component Services (Start > Control Panel > Administrative Tools > Component Services > Computers > My Computer > Distributed Transaction Coordinator > LocalDTC > Transaction Statistics).

When a transaction is promoted to a distriuted transaction, you see a change tot he total transaction count as wellas to the other
counters.

:: Creating a distributed transaction

För att skapa en distributed transaktion använder vi samma TransactionScope programmeringsmodell vi använde för att skapa en standard transaktion but add work to be performed on a different connection. Följande kod example uses two connections. Even though these connection objects use the same connection string, tehy are different connection objects, which will cause the single transaction to be promoted to a distributed transaction.

NOTE: Is Microsoft Distributed transaction coordinator running?

Verifiera att DTC Windows servcice är starta innan vi kör denna koden, annars får vi en exception som säger att MSDTC inte är tillgänglig.

var nwSetting = ConfigurationManager.ConnectionStrings["nw"];
var bulkSetting = ConfigurationManager.ConnectionStrings["BulkCopy"];
using (var ts = new TransactionScope())
{
	using(var cn = new SqlConnection())
	{
		cn.ConnectionString = nwSetting.ConnectionString;
		cn.Open();
		
		// work code here
		
		using(var cmd = cn.CreateCommand())
		{
			cmd.CommandText = "update products set unitsinstock = unitsinstock - 1 where productid=1";
			cmd.ExecuteNonQuery();
		}
	}
	
	using(var cn = new SqlConnection())
	{
		cn.ConnectionString = bulkSetting.ConnectionString;
		cn.Open();
		
		// work code here
		
		using(var cmd = cn.CreateCommand())
		{
			cmd.CommandText = "update products set unitsinstock = unitsinstock + 1 where productid = 2";
			cmd.ExecuteNonQuery();
		}
	}
	
	// if we made ti this far, commit
	// comment out ts.Complete() to simulate abort
	ts.Complete();
	
	var dt = new DataTable();
	using(var cn = new SqlConnection())
	{
		cn.ConnectionString = nwSetting.ConnectionString;
		cn.Open();
		
		// work code here
		
		using(var cmd 0 cn.CreateCommand())
		{
			cmd.CommandText = "select prouctid, unitsinstock from products where productid = 1";
			dt.Load(cmd.ExecuteReader());
		}
	}
	
	using(var cn = new SqlConnection())
	{
		cn.ConnectionString = bulkSetting.ConnectionString;
		cn.Open();
		
		// work code here
		
		using(var cmd = cn.CreateCommand())
		{
			cmd.CommandText = "select productid, unitsinstock from products where productid = 2";
			dt.Load(cmd.ExecuteReader());
		}
	}
	
	dataGridView2.DataSource = dt;
	

}

Eftersom koden innehåller flera uppkopplingar, the connections appear to the LTM as requiring multipe durable resource managers, and the transaction that was originally delegated to SQL Server 2005 and later is promtoed to a distributed transaction.











Lesson Summary
- transactions have 4 essential attributes: atomicity, consistency, isolation, durability (ACID)
- the attributes of consistnecy and isolation are impelmented by using the database's Lightweight Tranasction Manager (LTM)
locking mechanism, which keeps one transaction from affecting another
- Instead of operating with compelte isolation, you can modify the transaction isolation level; this reduces the amout of 
locking and increses scalability and performance
- to create a transaction, you can use the BeingTransaction() on the DbConnection class
- you can also use the TransactionScope class to create a promotable transaction
- a promotable transaction starts as an explicit transaction, but this transaction can be promoted to a distributed transaction
			 */

	}
}