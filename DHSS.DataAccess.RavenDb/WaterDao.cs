﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DHSS.Models;
using Raven.Client;

namespace DHSS.DataAccess.RavenDb
{
    public class WaterDao : IWaterDao
    {
        public List<Water> FetchAll()
        {
            List<Water> list;
            using(var session = DocumentStore.OpenSession())
            {
                var loadedWater = session.Load<Water>();
                list = loadedWater.ToList();
            }
            return list;
        }

        public IDocumentStore DocumentStore { get; set; }
    }
}
