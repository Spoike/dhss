﻿using System.Collections.Generic;
using DHSS.Models;

namespace DHSS.DataAccess
{
	public interface IResidencyStartReasonDao
	{
		List<ResidencyStartReason> FetchAll();
	}
}