﻿using System.Collections.Generic;
using DHSS.Models;

namespace DHSS.DataAccess
{
	public interface WaterDao
	{
		List<Water> FetchAll();
	}
}