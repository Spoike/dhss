﻿using System.Collections.Generic;
using DHSS.Models;

namespace DHSS.DataAccess
{
	public interface LavatoryDao
	{
		List<Lavatory> FetchAll();
	}
}