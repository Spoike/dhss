﻿using System.Collections.Generic;
using DHSS.Models;

namespace DHSS.DataAccess
{
	public interface SurveyStatusDao
	{
		List<SurveyStatus> FetchAll();
	}
}