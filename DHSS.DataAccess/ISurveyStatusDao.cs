﻿using System.Collections.Generic;
using DHSS.Models;

namespace DHSS.DataAccess
{
	public interface ISurveyStatusDao
	{
		List<SurveyStatus> FetchAll();
	}
}