﻿using System.Collections.Generic;
using DHSS.Models;

namespace DHSS.DataAccess
{
	public interface DistrictDao
	{
		List<District> FetchByTerritoryId(long territoryId);
	}
}