﻿using System.Collections.Generic;
using DHSS.Models;

namespace DHSS.DataAccess
{
	public interface FloorDao
	{
		List<Floor> FetchAll();
	}
}