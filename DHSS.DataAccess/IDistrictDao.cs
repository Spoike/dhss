﻿using System.Collections.Generic;
using DHSS.Models;

namespace DHSS.DataAccess
{
	public interface IDistrictDao
	{
		List<District> FetchByTerritoryId(long territoryId);
	}
}