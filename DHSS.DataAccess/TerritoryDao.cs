﻿using System.Collections.Generic;
using DHSS.Models;

namespace DHSS.DataAccess
{
	public interface TerritoryDao
	{
		List<Territory> FetchAll();
	}
}