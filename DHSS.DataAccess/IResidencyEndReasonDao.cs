﻿using System.Collections.Generic;
using DHSS.Models;

namespace DHSS.DataAccess
{
	public interface IResidencyEndReasonDao
	{
		List<ResidencyEndReason> FetchAll();
	}
}