﻿using System.Collections.Generic;
using DHSS.Models;

namespace DHSS.DataAccess
{
	public interface EducationDao
	{
		List<Education> FetchAll();
	}
}