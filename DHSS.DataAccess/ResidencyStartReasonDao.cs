﻿using System.Collections.Generic;
using DHSS.Models;

namespace DHSS.DataAccess
{
	public interface ResidencyStartReasonDao
	{
		List<ResidencyStartReason> FetchAll();
	}
}