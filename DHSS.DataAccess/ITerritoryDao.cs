﻿using System.Collections.Generic;
using DHSS.Models;

namespace DHSS.DataAccess
{
	public interface ITerritoryDao
	{
		List<Territory> FetchAll();
	}
}