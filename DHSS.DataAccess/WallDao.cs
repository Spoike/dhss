﻿using System.Collections.Generic;
using DHSS.Models;

namespace DHSS.DataAccess
{
	public interface WallDao
	{
		List<Wall> FetchAll();
	}
}