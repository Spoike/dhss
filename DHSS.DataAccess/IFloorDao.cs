﻿using System.Collections.Generic;
using DHSS.Models;

namespace DHSS.DataAccess
{
	public interface IFloorDao
	{
		List<Floor> FetchAll();
	}
}