﻿using System.Collections.Generic;
using DHSS.Models;

namespace DHSS.DataAccess
{
	public interface IWallDao
	{
		List<Wall> FetchAll();
	}
}