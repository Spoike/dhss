﻿using System.Collections.Generic;
using DHSS.Models;

namespace DHSS.DataAccess
{
	public interface IWaterDao
	{
		List<Water> FetchAll();
	}
}