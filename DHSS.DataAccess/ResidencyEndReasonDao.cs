﻿using System.Collections.Generic;
using DHSS.Models;

namespace DHSS.DataAccess
{
	public interface ResidencyEndReasonDao
	{
		List<ResidencyEndReason> FetchAll();
	}
}