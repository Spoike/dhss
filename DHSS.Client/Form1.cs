﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Transactions;
using System.Windows.Forms;
using DHSS.Models;

namespace DHSS.Client
{
	public partial class Form1 : Form
	{
		
		private List<Person> persons = new List<Person>();

		// When the application starts, it loads the offline XML file if it exists. It then checks whether
		// it can connect to SQL Server. If a connection can be made, the application then creates the
		// tables if they don'texist and attemptes to synchronize the client-side data with the server-side data.
		// When the application closes, a call is made to synchronize the data, and then the data set is serialized
		// to the offline XML file.

		private readonly string xsdFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "VehiclesRepairs.xsd");
		private readonly string xmlFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "VehiclesRepars.xml");
		private DataSet ds;

		private string cnString;

		private SqlDataAdapter daVehicles;
		private SqlDataAdapter daRepairs;

		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			cnString = ConfigurationManager.ConnectionStrings["db"].ConnectionString;

			InitializeDataAdapters();
			//CheckConnectivity();
			// Popualtes data set with a schema and data.
			PopulateDataSet();

			dgVehicles.DataSource = ds;
			dgVehicles.DataMember = "Vehicles";
			dgRepairs.DataSource = ds;
			dgRepairs.DataMember = "Vechicles.vehicles_repairs";
		}

		private void InitializeDataAdapters()
		{

			/*
			 In this method it took only a couple of lines of code to initialize the Vehicles data adapter.
			 * SqlCommandBuilder simplies the initialization but doesn't have th esame featuers that are
			 * available when writing all the code.
			 * 
			 * This becaomes apparent when initializing the Repairs data adapter. The Repairs table has an identity
			 * column that must be updated after inserts are done. The OUPUT keyword in SQL insert statement is equivalent
			 * of executing a SELECT statement on the row that was inserted, which updates the identity column in the client
			 * application. The ID column had negative values and needed to be upgraded to the actual values that are generated
			 * in SQL Server when the insert takes place.
			  
			 */
			cnString = ConfigurationManager.ConnectionStrings["db"].ConnectionString;

			// do vehicles with the SQL Command Builder
			daVehicles = new SqlDataAdapter("SELECT * FROM Vehicles", cnString);
			var bldVehicles = new SqlCommandBuilder(daVehicles);

			// do repairs by creating all commands
			var cn = new SqlConnection(cnString);
			var cmdSelectRepairs = cn.CreateCommand();
			var cmdUpdateRepairs = cn.CreateCommand();
			var cmdDeleteRepairs = cn.CreateCommand();
			var cmdInsertRepairs = cn.CreateCommand();

			cmdSelectRepairs.CommandText = "SELECT * FROM Repairs";

			cmdInsertRepairs.CommandText =
				"INSERT Repairs(VIN, Description, Cost) OUTPUT inserted.* VALUES(@VIN, @Description, @Cost);";

			cmdInsertRepairs.Parameters.Add("@VIN", SqlDbType.VarChar, 20, "VIN");

			cmdInsertRepairs.Parameters.Add("@Description", SqlDbType.VarChar, 60, "Description");

			cmdInsertRepairs.Parameters.Add("@Cost", SqlDbType.Money, 0, "Cost");

			cmdUpdateRepairs.CommandText = "UPDATE Repairs SET VIN=@VIN, Description=@Description, Cost=@Cost " +
			                               " WHERE ID=@OriginalID AND VIN=@OriginalID AND Description=@OriginalDescription AND Cost=@OriginalCost";

			cmdUpdateRepairs.Parameters.Add("@OriginalID", SqlDbType.Int, 0, "ID").SourceVersion = DataRowVersion.Original;

			cmdUpdateRepairs.Parameters.Add("@VIN", SqlDbType.VarChar, 20, "VIN");

			cmdUpdateRepairs.Parameters.Add("@OriginalVIN", SqlDbType.VarChar, 20, "VIN").SourceVersion = DataRowVersion.Original;

			cmdUpdateRepairs.Parameters.Add("@Description", SqlDbType.VarChar, 60, "Description");

			cmdUpdateRepairs.Parameters.Add("@OriginalDescription", SqlDbType.VarChar, 20, "Description").SourceVersion = DataRowVersion.Original;
			;

			cmdUpdateRepairs.Parameters.Add("@Cost", SqlDbType.Money, 0, "Cost");

			cmdUpdateRepairs.Parameters.Add("@OriginalCost", SqlDbType.Money, 0, "Cost").SourceVersion = DataRowVersion.Original;

			cmdDeleteRepairs.CommandText = "DELETE Repairs WHERE ID=@OriginalID " +
			                               " AND VIN=@OriginalVIN " +
			                               " AND Description = @OriginalDescdription " +
			                               " AND Cost=@OrginalCost";

			cmdDeleteRepairs.Parameters.Add("@OriginalID", SqlDbType.Int, 0, "ID").SourceVersion = DataRowVersion.Original;

			cmdDeleteRepairs.Parameters.Add("@OriginalVIN", SqlDbType.VarChar, 20, "VIN").SourceVersion = DataRowVersion.Original;

			cmdDeleteRepairs.Parameters.Add("@OriginalDescription", SqlDbType.VarChar, 20, "Description").SourceVersion = DataRowVersion.Original;


			cmdDeleteRepairs.Parameters.Add("@OriginalCost", SqlDbType.Money, 0, "Cost").SourceVersion = DataRowVersion.Original;

			daRepairs = new SqlDataAdapter(cmdSelectRepairs);
			daRepairs.InsertCommand = cmdInsertRepairs;
			daRepairs.UpdateCommand = cmdUpdateRepairs;
			daRepairs.DeleteCommand = cmdDeleteRepairs;
		}

		private bool CheckConnectivity()
		{
			try
			{
				using (var cn = new SqlConnection(cnString))
				{
					cn.Open();
					var version = cn.ServerVersion;
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
				return false;
			}
			return true;
		}

		private void CreateSchema()
		{
			ds = new DataSet("VechiclesRepairs");

			var vehicles = ds.Tables.Add("Vehicles");
			vehicles.Columns.Add("VIN", typeof (string));
			vehicles.Columns.Add("Make", typeof (string));
			vehicles.Columns.Add("Model", typeof (string));
			vehicles.Columns.Add("Year", typeof (int));
			vehicles.PrimaryKey = new DataColumn[] { vehicles.Columns["Id"] };

			var repairs = ds.Tables.Add("Repairs");
			var pk = repairs.Columns.Add("ID", typeof (int));
			pk.AutoIncrement = true;
			pk.AutoIncrementSeed = -1;
			pk.AutoIncrementStep = -1;
			repairs.Columns.Add("VIN", typeof (string));
			repairs.Columns.Add("Description", typeof (string));
			repairs.Columns.Add("Cost", typeof (decimal));
			repairs.PrimaryKey = new DataColumn[] { repairs.Columns["ID"] };

			ds.Relations.Add("vehicles_repairs", vehicles.Columns["VIN"], repairs.Columns["VIN"]);

			//MessageBox.Show("Schema created!");

			ds.WriteXmlSchema(xsdFile);
		}

		private void PopulateDataSet()
		{
			if(File.Exists(xsdFile))
			{
				ds = new DataSet();
				ds.ReadXmlSchema(xsdFile);
			}
			else
			{
				CreateSchema();
			}
			if(File.Exists(xmlFile))
			{
				ds.ReadXml(xmlFile, XmlReadMode.IgnoreSchema);
			}
			Synchronize();
		}

		private void Synchronize()
		{
			// Will be called when the application starts and when it ends.
			if (CheckConnectivity())
			{
				CreateTablesIfNotExist();
				SyncData();
			}
		}

		private void SyncData()
		{

			// If the updates are not successful, the transaction will roll back, and the changes will exist only
			// in the offline XML file until the next time synchronization is attempted.

			

			// send changes
			using (var tran = new TransactionScope())
			{
				try
				{
					daVehicles.Update(ds, "Vehicles");
					daRepairs.Update(ds, "Repairs");
					ds.AcceptChanges();
					tran.Complete();

				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message);
				}
			}

			// Sends client-side changes to the database and then receive all server-side changes.
			// Send changes.

			// Retrieve updates
			var tempVehicles = new DataTable();
			daVehicles.Fill(tempVehicles);
			ds.Tables["Vehicles"].Merge(tempVehicles);

			// Merge changes.
			var tempRepairs = new DataTable();
			daRepairs.Fill(tempRepairs);
			ds.Tables["Repairs"].Merge(tempRepairs);

		}

		private void CreateTablesIfNotExist()
		{
			try
			{
				using(var cn = new SqlConnection(cnString))
				using(var cmd = cn.CreateCommand())
				{
					cn.Open();
					cmd.CommandText = "IF NOT EXISTS ( " +
					                  " SELECT * FROM sys.Tables WHERE Name='Vehicles') " +
					                  " CREATE TABLE Vechicles( " +
					                  " VIN varchar(20) PRIMARY KEY, " +
					                  " Make varchar(20), " +
					                  " Model varchar(20), Year int)";
					cmd.ExecuteNonQuery();
					cmd.CommandText = "IF NOT EXISTS ( " +
					                  " SELECT * From sys.Tables WHERE Name='Repairs') " +
					                  " CREATE TABLE Repairs ( " +
					                  " ID int IDENTITY PRIMARY KEY, " +
					                  " VIN varchar(20), " +
					                  " Description varchar(60), " +
					                  " Cost Money)";
					cmd.ExecuteNonQuery();
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
				throw;
			}
		}

		private void Form1_FormClosing(object sender, FormClosingEventArgs e)
		{
			Synchronize();
			ds.WriteXml(xmlFile, XmlWriteMode.DiffGram);
		}
	}
}
