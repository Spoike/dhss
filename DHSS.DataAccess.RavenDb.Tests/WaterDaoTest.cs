﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DHSS.Models;
using NUnit.Framework;
using Raven.Client;
using Moq;

namespace DHSS.DataAccess.RavenDb
{
    [TestFixture]
    public class WaterDaoTest
    {
        // CUT
        private WaterDao _waterDao;

        // Mocks
        private Mock<IDocumentStore> _mockedStore;
        private Mock<IDocumentSession> _mockedSession;

        [SetUp]
        public void SetUp()
        {
            _waterDao = new WaterDao();

            _mockedStore = new Mock<IDocumentStore>();
            _mockedSession = new Mock<IDocumentSession>();

            _waterDao.DocumentStore = _mockedStore.Object;
        }

        [Test]
        public void WhenCallingFetchAllShouldOpenSessionOnDocumentStore()
        {
            // ARRANGE
            _mockedStore.Setup(store => store.OpenSession())
                .Returns(_mockedSession.Object);

            // ACT
            _waterDao.FetchAll();

            // ASSERT
            _mockedStore.Verify(store => store.OpenSession(), Times.AtLeastOnce());
        }

        [Test]
        public void WhenCallingFetchAllShouldFetchItFromTheDocumentSession()
        {
            // ARRANGE
            var waterArray = new [] {new Mock<Water>().Object};
            _mockedStore.Setup(store => store.OpenSession())
                .Returns(_mockedSession.Object);
            _mockedSession.Setup(session => session.Load<Water>()).Returns(waterArray);

            // ACT
            _waterDao.FetchAll();

            // ASSERT
            _mockedSession.Verify(session => session.Load<Water>(), Times.AtLeastOnce());
        }

        [Test]
        public void WhenCallingFetchAllShouldFetchItAndReturnIt()
        {
            // ARRANGE
            var w = new Mock<Water>().Object;
            var waterArray = new[] { w };
            _mockedStore.Setup(store => store.OpenSession())
                .Returns(_mockedSession.Object);
            _mockedSession.Setup(session => session.Load<Water>()).Returns(waterArray);

            // ACT
            var actualList = _waterDao.FetchAll();

            // ASSERT
            Assert.That(actualList, Contains.Item(w));
        }

        [Test]
        public void WhenUsingSessionShouldDisposeItAtTheEnd()
        {
            // ARRANGE
            _mockedStore.Setup(store => store.OpenSession())
                .Returns(_mockedSession.Object);

            // ACT
            _waterDao.FetchAll();

            // ASSERT
            _mockedSession.Verify(session => session.Dispose(), Times.AtLeastOnce());
        }
    }
}
