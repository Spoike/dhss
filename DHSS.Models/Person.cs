﻿using System;

namespace DHSS.Models
{
	public class Person
	{
		public long PersonId { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public DateTime BirthDate { get; set; }
		public string Sex { get; set; }
		public Education Education { get; set; }
		public Occupation Occupation { get; set; }
	}
}