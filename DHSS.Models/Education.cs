﻿namespace DHSS.Models
{
	public class Education
	{
		public long EducationId { get; set; }
		public string Name { get; set; }
	}
}