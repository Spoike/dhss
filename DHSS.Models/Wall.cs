﻿namespace DHSS.Models
{
	public class Wall
	{
		public long WallId { get; set; }
		public string Name { get; set; }
	}
}