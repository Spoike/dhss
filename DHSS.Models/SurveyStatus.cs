﻿namespace DHSS.Models
{
	public class SurveyStatus
	{
		public long SurveyStatusId { get; set; }
		public string Name { get; set; }
	}
}