﻿namespace DHSS.Models
{
	public abstract class EpisodeEndReason
	{
		public string Name { get; set; }
	}
}