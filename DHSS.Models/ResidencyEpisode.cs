﻿namespace DHSS.Models
{
	class ResidencyEpisode : Episode
	{
		public long ResidencyEpisodeId { get; set; }
		public Residence Residence { get; set; }
		public Person Person { get; set; }
	}
}