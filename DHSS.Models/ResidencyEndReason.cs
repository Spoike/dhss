﻿namespace DHSS.Models
{
	public class ResidencyEndReason
	{
		public long ResidencyEpisodeEndCauseId { get; set; }
		public string Name { get; set; }
	}
}