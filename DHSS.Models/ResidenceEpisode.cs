﻿namespace DHSS.Models
{
	public class ResidencyEpisode : Episode
	{
		public long ResidencyEpisodeId { get; set; }
		public ResidencyStartReason EpisodeStartCause { get; set; }
		public ResidencyEndReason EpisodeEndCause { get; set; }
	}
}