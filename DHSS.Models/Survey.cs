﻿namespace DHSS.Models
{
	public class Survey
	{
		public long SurveyId { get; set; }
		public SurveyStatus Status { get; set; }
		public User Interviewer { get; set; }
		public Census Census { get; set; }
		public Residence Residence { get; set; }
	}
}