﻿namespace DHSS.Models
{
	public class Address
	{
		public long AddressId { get; set; }
		public District District { get; set; }
		public int Block { get; set; }
		public string Code { get; set; }
	}
}