﻿using System;

namespace DHSS.Models
{
	public abstract class Episode
	{
		public long EpisodeId { get; set; }
		public DateTime StartDate { get; set; }
		public EpisodeStartReason StartReason { get; set; }
		public DateTime EndDate { get; set; }
		public EpisodeEndReason EndReason { get; set; }
	}
}