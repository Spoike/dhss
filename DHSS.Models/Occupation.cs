﻿namespace DHSS.Models
{
	public class Occupation
	{
		public long OccupationId { get; set; }
		public string Name { get; set; }
	}
}