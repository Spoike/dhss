﻿namespace DHSS.Models
{
	public class District
	{
		public long DistrictId { get; set; }
		public string Name { get; set; }
		public string Code { get; set; }
		public Territory Territory { get; set; }
	}
}