﻿namespace DHSS.Models
{
	public class Territory
	{
		public long TerritoryId { get; set; }
		public string Name { get; set; }
		public string Code { get; set; }
	}
}