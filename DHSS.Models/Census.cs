﻿using System;

namespace DHSS.Models
{
	public class Census
	{
		public long CensusId { get; set; }
		public string Name { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
	}
}