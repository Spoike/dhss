﻿namespace DHSS.Models
{
	public class Residence
	{
		public long ResidenceId { get; set; }
		public bool Electricity { get; set; }
		public Wall Wall { get; set; }
		public Floor Floor { get; set; }
		public Water Water { get; set; }
		public Lavatory Lavatory { get; set; }
		public Address Address { get; set; }
	}
}