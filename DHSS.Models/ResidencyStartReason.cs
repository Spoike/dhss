﻿namespace DHSS.Models
{
	public class ResidencyStartReason
	{
		public long ResidencyEpisodeStartCauseId { get; set; }
		public string Name { get; set; }
	}
}