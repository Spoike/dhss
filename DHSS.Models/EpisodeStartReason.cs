﻿namespace DHSS.Models
{
	public abstract class EpisodeStartReason
	{
		public long EpisodeStartReasonId { get; set; }
		public string Name { get; set; }
	}
}