﻿using System.Collections.Generic;

namespace DHSS.Models
{
	public class User
	{
		public long UserId { get; set; }
		public string Username { get; set; }
		public string Password { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public List<Role> Roles { get; set; }
	}
}