﻿namespace DHSS.Models
{
	public class Floor
	{
		public long FloorId { get; set; }
		public string Name { get; set; }	
	}
}