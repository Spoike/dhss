﻿using System.Collections.Generic;
using DHSS.DataAccess.SqlServer;
using DHSS.Models;
using NUnit.Framework;

namespace DHSS.DataAccess.Tests.SqlServer
{
	[TestFixture]
	public class TerritoryDaoTests
	{
		[Test]
		public void FetchAll()
		{
			var territoryDao = new TerritoryDao();
			List<Territory> territoryList = territoryDao.FetchAll();
			Assert.AreEqual(3, territoryList.Count);
		}
	}
}