﻿using System.Collections.Generic;
using DHSS.DataAccess.SqlServer;
using DHSS.Models;
using NUnit.Framework;

namespace DHSS.DataAccess.Tests.SqlServer
{
	class WaterSqlServerDaoTests
	{
		[Test]
		public void FetchAll()
		{
			WaterDao waterDao = new WaterSqlServerDao();
			List<Water> waterList = waterDao.FetchAll();
			Assert.AreEqual(6, waterList.Count);
		}
	}
}