﻿using System.Collections.Generic;
using DHSS.DataAccess.SqlServer;
using DHSS.Models;
using NUnit.Framework;

namespace DHSS.DataAccess.Tests.SqlServer
{
	class WaterDaoTests
	{
		[Test]
		public void FetchAll()
		{
			IWaterDao waterDao = new WaterDao();
			List<Water> waterList = waterDao.FetchAll();
			Assert.AreEqual(6, waterList.Count);
		}
	}
}