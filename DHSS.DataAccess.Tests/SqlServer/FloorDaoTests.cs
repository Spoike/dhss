﻿using System.Collections.Generic;
using DHSS.DataAccess.SqlServer;
using DHSS.Models;
using NUnit.Framework;

namespace DHSS.DataAccess.Tests.SqlServer
{
	class FloorDaoTests
	{
		[Test]
		public void FetchAll()
		{
			IFloorDao floorDao = new FloorDao();
			List<Floor> floorList = floorDao.FetchAll();
			Assert.AreEqual(5, floorList.Count);
		}
	}
}