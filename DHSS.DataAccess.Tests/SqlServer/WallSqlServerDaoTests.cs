﻿using System.Collections.Generic;
using DHSS.DataAccess.SqlServer;
using DHSS.Models;
using NUnit.Framework;

namespace DHSS.DataAccess.Tests.SqlServer
{
	[TestFixture]
	public class WallSqlServerDaoTests
	{
		[Test]
		public void FetchAll()
		{
			WallDao wallDao = new WallSqlServerDao();
			List<Wall> wallList = wallDao.FetchAll();
			Assert.AreEqual(5, wallList.Count);
		}
	}
}