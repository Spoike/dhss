﻿using System.Collections.Generic;
using DHSS.DataAccess.SqlServer;
using DHSS.Models;
using NUnit.Framework;

namespace DHSS.DataAccess.Tests.SqlServer
{
	class LavatorySqlServerDaoTests
	{
		[Test]
		public void FetchAll()
		{
			LavatoryDao lavatoryDao = new LavatorySqlServerDao();
			List<Lavatory> lavatoryList = lavatoryDao.FetchAll();
			Assert.AreEqual(6, lavatoryList.Count);
		}
	}
}