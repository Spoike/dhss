﻿using System.Collections.Generic;
using DHSS.DataAccess.SqlServer;
using DHSS.Models;
using NUnit.Framework;

namespace DHSS.DataAccess.Tests.SqlServer
{
	class FloorSqlServerDaoTests
	{
		[Test]
		public void FetchAll()
		{
			FloorDao floorDao = new FloorSqlServerDao();
			List<Floor> floorList = floorDao.FetchAll();
			Assert.AreEqual(5, floorList.Count);
		}
	}
}