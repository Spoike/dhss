﻿using System.Collections.Generic;
using DHSS.DataAccess.SqlServer;
using DHSS.Models;
using NUnit.Framework;

namespace DHSS.DataAccess.Tests.SqlServer
{
	class LavatoryDaoTests
	{
		[Test]
		public void FetchAll()
		{
			ILavatoryDao lavatoryDao = new LavatoryDao();
			List<Lavatory> lavatoryList = lavatoryDao.FetchAll();
			Assert.AreEqual(6, lavatoryList.Count);
		}
	}
}