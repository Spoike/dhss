﻿using System.Collections.Generic;
using DHSS.DataAccess.SqlServer;
using DHSS.Models;
using NUnit.Framework;

namespace DHSS.DataAccess.Tests.SqlServer
{
	[TestFixture]
	public class DistrictSqlServerDaoTests
	{
		[Test]
		public void FetchByTerritoryId()
		{
			DistrictDao districtDao = new DistrictSqlServerDao();
			List<District> districtList = districtDao.FetchByTerritoryId(1);
			Assert.AreEqual(2, districtList.Count);
		}
	}
}