﻿using System.Collections.Generic;
using DHSS.DataAccess.SqlServer;
using DHSS.Models;
using NUnit.Framework;

namespace DHSS.DataAccess.Tests.SqlServer
{
	[TestFixture]
	public class TerritorySqlServerDaoTests
	{
		[Test]
		public void FetchAll()
		{
			TerritoryDao territoryDao = new TerritorySqlServerDao();
			List<Territory> territoryList = territoryDao.FetchAll();
			Assert.AreEqual(3, territoryList.Count);
		}
	}
}